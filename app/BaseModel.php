<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    public function user(){
        return $this->belongsTo(User::class);
    }

}
