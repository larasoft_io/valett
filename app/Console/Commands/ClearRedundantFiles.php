<?php

namespace App\Console\Commands;

use App\Order;
use App\RedundantFile;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;


class ClearRedundantFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Redundant CSV Files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $redundant_files = RedundantFile::where('created_at', '<=', Carbon::now()->subDay(1))->get();

        foreach($redundant_files as $redundant_file)
        {
            $file = storage_path() . '/csv/' .$redundant_file->file_name;

            if(file_exists($file)) {
                File::delete($file);
            }

            $redundant_file->delete();
        }

         $orders = Order::where('created_at','<=',Carbon::now()->subDay(30))->get();
         foreach($orders as $order){
            $file = storage_path() . '/purchased_files/' .$order->file_path.'.'.$order->file_format;

            if(file_exists($file)) {
                File::delete($file);
            }
         }
    }
}
