<?php

namespace App\Http\Controllers;

use App\Jobs\ExportData;
use App\Order;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Stripe\Charge;
use Maatwebsite\Excel\Facades\Excel;
use \Stripe\Stripe;
use \Stripe\Customer;

class CheckoutController extends Controller
{
    public function getRecords(Request $request)
    {
        $record_ids = $request->record_ids;
        $total_cost = $request->total_cost;

        session(['step2' => ['ids' => $record_ids]]);


        session(['cost' => $total_cost]);
    }

    public function storeCheckoutStep1(Request $request)
    {
        session(['step1' => $request->all()]);
    }

    public function storeCheckoutStep2(Request $request)
    {
//        session('stripe_token', $request->token);

        session(['stripe_token' => $request->token]);
        session(['method' => $request->payment_method]);

    }


    public function getCheckoutRecords(Request $request)
    {
        $count = count(session('step2.ids'));
        $total_cost = session('cost');

        return response()->json([
            'count' => $count,
            'total_cost' => $total_cost,
//            'is_email' => (session()->has('step1.email') && session('step1.email') != '') ? true : false,
            'is_email_checked' => (session()->has('step1.email_checked') && session('step1.email_checked') == true) ? true : false
        ]);
    }

    public function storeCheckoutStep3(Request $request)
    {

        Stripe::setApiKey(env('STRIPE_SECRET'));

        $records_count = count(session('step2.ids'));

        $total_price = session('cost');              // Records Cost
        $total = $request->total;                   // Records Cost + Email Cost

        if ($request->is_email_checked == true) {
            $is_email = 1;
            $email_cost = config('settings.email_cost');
        } else {
            $is_email = 0;
            $email_cost = 0;
        }

        if (session('method') == 0) {

            // Case 1: Existing Card
//            $customer_id = auth()->user()->orders()->latest()->first()->stripe_customer_id;
            $customer_id = auth()->user()->stripe_id;

            $charge = Charge::create(array(
                "amount" => round($total * 100),
                "currency" => "usd",
                "customer" => $customer_id
            ));

        } elseif (session('method') == 1) {
            // Case 2: New Card
            $customer = Customer::create(["email" => auth()->user()->email, 'source' => session('stripe_token')]);

            $customer_id = $customer->id;

            $charge = Charge::create(array(
                "amount" => round($total * 100),
                "currency" => "usd",
                "customer" => $customer_id
            ));
        }

        if (isset($charge) && $charge->paid == true) {
            $order = $this->makeOrder($records_count, $total_price, $total, $is_email, $charge, $email_cost);

            $data['user_id'] = auth()->id();
            $data['user_name'] = auth()->user()->name;
            $data['record_ids'] = session('step2.ids');
            $data['table_name'] = session('step1.table');
            $data['file_format'] = session('step1.fileFormat');
            $data['field_names'] = session('step1.fieldNames');
            $data['file_name'] = auth()->id() . '_' . time() . '_file';
            $data['url'] = url("/orders/{$order->id}/file/download");

            $this->dispatch(new ExportData($data));

        }
    }

    /**
     * @param $charge
     * @return $order
     */
    public function makeOrder($records_count, $total_price, $total, $is_email, $charge, $email_cost)
    {
        $customer_id = $charge->source->customer;
        $last_four = $charge->source->last4;

        // save order
        $fieldNames = session('step1.fieldNames');
        $table = session('step1.table');
//        $email = session('step1.email');
        $fileFormat = session('step1.fileFormat');
        $record_ids = session('step2.ids');
        $stripe_customer_id = $customer_id;
        $card_last_four = $last_four;

        $fields = [];
        $ids = [];

        foreach ($fieldNames as $fieldName) {

            array_push($fields, $fieldName);
        }

        foreach ($record_ids as $record_id) {
            array_push($ids, $record_id);
        }

        $order = Order::create([
            'records_count' => $records_count,
            'total_price' => $total_price,
            'total' => $total,
            'is_email' => $is_email,
            'user_id' => auth()->user()->id,
            'email_cost' => $email_cost,
            'table_name' => $table,
            'file_format' => $fileFormat,
            'selected_columns' => implode(',', $fields),
            'stripe_customer_id' => $stripe_customer_id,
            'card_last_four' => $card_last_four,
            'file_path' => auth()->id() . '_' . time() . '_file'
        ]);

        // Todo: Store Order details here

        $price_0_to_7 = Setting::whereName('price_0_to_7')->first()->value;
        $price_7_to_14 = Setting::whereName('price_7_to_14')->first()->value;
        $price_14_to_30 = Setting::whereName('price_14_to_30')->first()->value;
        $price_greater_30 = Setting::whereName('price_greater_30')->first()->value;

        $vendor_user_ids = session('order_details');

        foreach ($vendor_user_ids as $id => $data) {

            $user_id = $id;
            $range1 = isset($data['range1_count']) ? $data['range1_count'] : 0;
            $range2 = isset($data['range2_count']) ? $data['range2_count'] : 0;
            $range3 = isset($data['range3_count']) ? $data['range3_count'] : 0;
            $range4 = isset($data['range4_count']) ? $data['range4_count'] : 0;

            $order->orderDetails()->create([
                'user_id' => $user_id,
                'no_of_records' => $range1 + $range2 + $range3 + $range4,
                'total' => ($range1 * $price_0_to_7) + ($range2 * $price_7_to_14) + ($range3 * $price_14_to_30) + ($range4 * $price_greater_30)
            ]);
        }

        return $order;
    }

    public function getRecordsCountCost()
    {
        $count = count(session('step2.ids'));
        $total_cost = session('cost');

        return response()->json([
            'recordsCount' => $count,
            'totalPrice' => $total_cost,
//            'is_email' => (session()->has('step1.email') && session('step1.email') != '') ? true : false,
            'is_email_checked' => (session()->has('step1.email_checked') && session('step1.email_checked') == true) ? true : false,

            'email' => session('step1.email')

        ]);
    }

    public function checkPreviousOrders()
    {
        return auth()->user()->orders()->count();
    }

}
