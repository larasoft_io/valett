<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('subscribed');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        if (auth()->user()->hasRole('buyer') && !request()->has('table')){
            return redirect('/dashboard?table='.array_keys(auth()->user()->getDBTables(auth()->user()))[0]);
        }

        return view('home');
    }

    public function store_settings(Request $request)
    {
         $price_0_to_7 = Setting::where('name','price_0_to_7')->first();
         $price_7_to_14 = Setting::where('name','price_7_to_14')->first();
         $price_14_to_30 = Setting::where('name','price_14_to_30')->first();
         $price_greater_30 = Setting::where('name','price_greater_30')->first();

        if($price_0_to_7){
            $price_0_to_7->update(['value' => $request->price_0_to_7]);
        }
        if($price_7_to_14){
            $price_7_to_14->update(['value' => $request->price_7_to_14]);
        }
        if($price_14_to_30){
            $price_14_to_30->update(['value' => $request->price_14_to_30]);
        }
        if($price_greater_30){
            $price_greater_30->update(['value' => $request->price_greater_30]);
        }

        return redirect('/spark/kiosk#/pricing');

    }
}
