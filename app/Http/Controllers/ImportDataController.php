<?php

namespace App\Http\Controllers;

use App\ImportedFile;
use App\Jobs\ImportVendorData;
use App\RedundantFile;
use App\Traits\ImportCsvTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportDataController extends Controller
{
    use ImportCsvTrait;

    public function import($step)
    {
        if ($step == 1) {
            $tables = $this->getDBTables(auth()->user());

            return view("import.step{$step}", compact('tables'));
        } elseif ($step == 2) {
            $table_fields = [];

            if (session('step1.new_table') != 1) {
                $table_fields = $this->getCombinedTableFields(session('step1.table'));
            }

            $file = storage_path('csv/' . session('step1.filename'));

            $f = fopen($file, 'r');
            $line = str_replace(' ', '', trim(fgets($f)));
            fclose($f);

            $csv_fields = explode(',', $line);

            session(['csv_fields' => $csv_fields]);

            return view("import.step{$step}", compact('table_fields', 'csv_fields'));
        } elseif ($step == 3) {
            flash("Great Work! We are just importing your data and will notify you when it's ready.", 'success');
            return view("import.step{$step}");
        }

    }

    public function storeStep1(Request $request)
    {
        if ($request->has('new_table')) {
            session()->flash('new_table', 1);
            $this->validate($request, ['table_name' => 'required'], ['table_name.required' => 'The type name field is required.']);
        } else {
            $this->validate($request, ['table' => 'required'], ['table.required' => 'Please select a type.']);
        }

        $content = File::get($request->file('file'));
        $original_name = $request->file('file')->getClientOriginalName();
        $extension = $request->file('file')->getClientOriginalExtension();
        $size = $request->file('file')->getSize();

        $existing_file = ImportedFile::where('file_name', $original_name)
                            ->where('file_extension', $extension)
                            ->where('file_size', $size)
                            ->where('user_id', auth()->id())
                            ->first();

        if ($existing_file) {
            flash("This file looks like already been imported. Please select another file.", 'danger');
            return redirect()->back();
        }

        // Create model dynamically
        if ($request->has('new_table')) {
            $model = $this->getModelName($request->table_name);

            $code = '<?php 
namespace App\\DataModels;

use App\\BaseModel;

class '.$model.' extends BaseModel {
    protected $table = "'.$request->table_name.'";
}
';
            $fp = fopen(app_path("DataModels/$model.php"),'w');

            file_put_contents(app_path("DataModels/$model.php") ,$code);

            fclose($fp);

//            if (!class_exists($model)) {
//                Artisan::call("make:model", ['name' => $model]);
//
//                $filename = app_path() . '/' . $model . '.php';
//                $string_to_replace = "extends Model";
//                $replace_with = "extends BaseModel";
//                $this->replace_string_in_file($filename, $string_to_replace, $replace_with);
//
//                $string_to_replace = "{";
//                $replace_with = '{
//                            protected $table = "' . $request->table_name . '";';
//                $this->replace_string_in_file($filename, $string_to_replace, $replace_with);
//            }
        }

        $filename = auth()->id() . '_' . time() . '_file.csv';

        Storage::disk('csv')->put($filename, $content);

        RedundantFile::create([
            'user_id' => auth()->id(),
            'file_name' => $filename
        ]);

        ImportedFile::create([
            'user_id' => auth()->id(),
            'file_name' => $original_name,
            'file_extension' => $extension,
            'file_size' => $size
        ]);

        $data = $request->except('file');

        $data['filename'] = $filename;

        session(['step1' => $data]);

        return redirect()->route('vendor.import.data', 2);
    }


    public function storeStep2(Request $request)
    {
        if (count(array_filter($request->fields)) == 0){
            flash('Please define at least one table field.', 'danger');
            return redirect()->back();
        }

        $table_fields = $request->fields;
        $csv_fields = session('csv_fields');
        $table_name = session('step1.new_table') == 1 ? session('step1.table_name') : session('step1.table');
        if (!empty($request->new_fields)) {
            $new_fields_array = $request->new_fields;
            $myArray = array_filter($new_fields_array);
            $new_fields = array_diff($myArray, array(''));
            $checkDuplicationFields = count($new_fields) !== count(array_unique($new_fields));

            if ($checkDuplicationFields == true) {
                flash('Adding duplicate fields not allowed!', 'danger');
                return redirect()->back();
            }

            $all_fields_table = $this->getAllTableFields($table_name);
            $result = array_intersect($new_fields, $all_fields_table);

            if (!empty($result)) {
                flash('These fields already exist!', 'danger');
                return redirect()->back();
            }
        }

        if (session('step1.new_table') == 1) {
            Schema::create($table_name, function ($table) use ($table_fields) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                foreach ($table_fields as $index => $field) {
                    if ($table_fields[$index] != '') {
                        $table->string($field)->nullable();
                    }
                }
                $table->timestamps();

            });

            Schema::create($table_name."_suppression", function ($table) use ($table_fields, $table_name) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer(str_singular($table_name).'_id')->unsigned();
                $table->foreign(str_singular($table_name).'_id')->references('id')->on($table_name);

                $table->timestamps();
            });
        }

        if (!empty($new_fields)) {
            Schema::table(session('step1.table'), function ($table) use ($new_fields) {
                foreach ($new_fields as $index => $field) {
                    $table->string($field)->nullable();
                }
            });
        }

        $file = storage_path('csv/' . session('step1.filename'));

        if (File::exists($file)) {
            $data['file_name'] = session('step1.filename');
            $data['csv_fields'] = $csv_fields;
            $data['table_fields'] = $table_fields;
            $data['user_id'] = auth()->id();
            $data['table_name'] = $table_name;

            $this->dispatch(new ImportVendorData($data));
        }

        return redirect()->route('vendor.import.data', 3);
    }

    public function checkDatabaseName(Request $request)
    {
        $table_name = $request->table_name;
        $db_tables = DB::select('SHOW TABLES');

        $property = "Tables_in_" . env('DB_DATABASE');

        $tables = array_map(create_function('$o', 'return $o->' . $property . ';'), $db_tables);
        if (in_array($table_name, $tables)) {
            return 'success';
        }

    }

    function replace_string_in_file($filename, $string_to_replace, $replace_with)
    {
        $content = file_get_contents($filename);
        $content_chunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $content_chunks);
        file_put_contents($filename, $content);
    }

}
