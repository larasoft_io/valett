<?php

namespace App\Http\Controllers;

use App\Jobs\ImportBuyerSuppression;
use App\Order;
use App\RedundantFile;
use App\Traits\ImportCsvTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportSuppressionController extends Controller
{
    use ImportCsvTrait;

    protected $csv_fields = [];

    public function import($step)
    {
        if ($step == 1) {

            $tables = $this->getDBTables('suppression');

            return view("suppression.step$step", compact('tables'));
        } elseif ($step == 2) {
            $table_fields = $this->getCombinedTableFields(session('step1.table'));

            $file = storage_path('csv/' . session('step1.filename'));

            Excel::load($file, function ($reader) {

                $results = $reader->get();
                $headers = array_keys($results->first()->toArray());

                $this->csv_fields = $headers;

            });

            session(['csv_fields' => $this->csv_fields]);

            $csv_fields = $this->csv_fields;

            return view("suppression.step$step", compact('table_fields', 'csv_fields'));
        } elseif ($step == 3) {

            $default_table = array_keys(auth()->user()->getDBTables(auth()->user()))[0];

            return view("suppression.step$step", compact('default_table'));
        }
    }

    public function storeStep1(Request $request)
    {
        $this->validate($request, ['table' => 'required']);

        $table = $request->table;

        $content = File::get($request->file('file'));

         $extension = $request->file('file')->getClientOriginalExtension();
         $original_file_name = $request->file('file')->getClientOriginalName();

        $filename = auth()->id() . '_' . time() . '_suppression_file.' . $extension;

        Storage::disk('csv')->put($filename, $content);

        $data['table'] = $table;
        $data['filename'] = $filename;
        $data['file_original_name'] = $original_file_name;

        RedundantFile::create([
            'user_id' => auth()->id(),
            'file_name' => $filename
        ]);

        session(['step1' => $data]);

        return redirect()->route('import.suppression', 2);
    }

    public function storeStep2(Request $request)
    {
        $csv_field = $request->csv_field;
        $table = explode("_", session('step1.table'))[0];
        $table_fields = $this->getCombinedTableFields($table);

        $file = storage_path('csv/' . session('step1.filename'));

        $data['file'] = $file;
        $data['table'] = $table;
        $data['file_original_name'] = session('step1.file_original_name');
        $data['csv_field'] = $csv_field;
        $data['table_fields'] = array_flatten($table_fields);
        $data['user_id'] = auth()->id();

        $this->dispatch(new ImportBuyerSuppression($data));

        flash('Congratulations! Your suppression has been processed successfully.', 'success');

        return redirect()->route('import.suppression', 3);
    }


    public function getSuppressionFiles()
    {
        $suppression_files = auth()->user()->SuppressionInformation;
        return $suppression_files;
    }

}
