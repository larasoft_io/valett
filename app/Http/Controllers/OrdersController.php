<?php

namespace App\Http\Controllers;

use App\Jobs\ExportData;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Stripe\Charge;
use Maatwebsite\Excel\Facades\Excel;


class OrdersController extends Controller
{
    public function index()
    {
        $orders = auth()->user()->orders()->orderBy('created_at', 'desc')->paginate(15);

//        foreach ($orders as $order){
//            $order['file_name'] = $order->file_path;
//            $order['download_url'] = url("buyer/orders/{$order->id}/file/download");
//            $order['purchased_date'] = $order->created_at->format('jS M, Y');
//            $order['is_expired'] = $order->created_at < Carbon::parse('-30 days');
//        }

        return $orders;
    }


    public function invoice(Request $request)
    {
        $invoice = Order::where('id', $request->order_id)->first();


        return response()->json([
            'invoice' => $invoice,
            'file_name' => $invoice->file_path,
            'table_name' => $invoice->table_name,
            'download_url' => url("/orders/{$invoice->id}/file/download"),
            'is_expired' =>  $invoice->created_at < Carbon::parse('-30 days'),
            'is_email' => ($invoice->is_email == 1) ? true : false,
        ]);
    }


    public function vendor_orders()
    {
         return $order_vendors = auth()->user()->orderByVendors()->orderBy('created_at', 'desc')->paginate(15);
    }

    public function downloadFile($id){
        $order = Order::findOrFail($id);

        $file = storage_path('purchased_files').'/'.$order->file_path.'.'.$order->file_format;

        if (File::exists($file)){
            return response()->download($file);
        }else{
            abort(404);
        }
    }
}
