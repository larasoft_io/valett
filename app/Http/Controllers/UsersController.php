<?php

namespace App\Http\Controllers;

use App\Mail\InviteBuyer;
use App\RedundantFile;
use App\SearchQuery;
use App\Setting;
use App\Traits\ImportCsvTrait;
use App\User;
use App\ZipCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Laravel\Spark\Invitation;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;


class UsersController extends Controller
{
    use ImportCsvTrait;

    public function saveQuery(Request $request)
    {
        Log::info($request->all());

//        return $request->vendors;
        $fields = serialize($request->fields);
        $table_name = $request->table;
        $queryName = $request->queryName;

        $vendor_ids = [];

        foreach ($request->vendors as $vendor) {

            array_push($vendor_ids, $vendor['value']);
        }

        $query = SearchQuery::where('name', $queryName)->first();
        if (!$query) {
            SearchQuery::create([
                'name' => $queryName,
                'table_name' => $table_name,
                'fields' => $fields,
                'vendor_ids' => implode(',', $vendor_ids),
                'user_id' => auth()->id()
            ]);

            return response()->json(['success' => 1, 'message' => 'Query has been saved successfully.']);
        } else {
            return response()->json(['success' => 0, 'message' => 'Query with this name already exists.']);
        }

    }

    public function searchQuery(Request $request)
    {
        $search_queries = auth()->user()->searchQueries()->where('table_name', $request->table_name)->pluck('name', 'id');

        $options = [];
        foreach ($search_queries as $id => $name) {
            array_push($options, ['name' => $name, 'value' => $id]);
        }

        return response()->json([
            'options' => $options
        ]);
    }

    public function previousQueryData(Request $request)
    {
        $previous_query_data = SearchQuery::where('id', $request->id)->first();

        $vendors_data = User::whereIn('id', explode(',', $previous_query_data->vendor_ids))->get();

        $vendors = [];

        foreach ($vendors_data as $vendor_data) {
            array_push($vendors, ['name' => $vendor_data->name, 'value' => $vendor_data->id]);
        }

        $fields = unserialize($previous_query_data->fields);

        $fields_data = [];
        foreach ($fields as $field) {
            array_push($fields_data, ['name' => $field['name'], 'value' => $field['value'], 'formatted_name' => $this->dataStandardisation($field['name'])]);
        }

        return response()->json([
            'fields' => $fields_data,
            'vendors' => $vendors
        ]);
    }

    public function checkFileRedundant()
    {
        $file_name = session('step1.filename');
        $file = storage_path() . '/csv/' . $file_name;

        if (file_exists($file)) {
            File::delete($file);
            $get_file_row = RedundantFile::where('file_name', $file_name)->first();
            $get_file_row->delete();
        }
    }

    public function getFieldsFromTable(Request $request)
    {
        $fields = [];

        if ($request->has('step1')) {
            foreach ($this->getTableFields($request->table_name) as $field_name) {
                array_push($fields, ['name' => $field_name, 'value' => '', 'formatted_name' => $this->dataStandardisation($field_name)]);
            }
        } else {
            if ($request->table_name == "mortgage") {
                array_push($fields, ['name' => 'state', 'value' => [], 'formatted_name' => 'State']);
                array_push($fields, ['name' => 'city', 'value' => [], 'formatted_name' => 'City']);
                array_push($fields, ['name' => 'amount', 'value' => '', 'formatted_name' => 'Amount']);
                array_push($fields, ['name' => 'credit_rating', 'value' => '', 'formatted_name' => 'Credit Rating']);
            } elseif ($request->table_name == "insurances") {
                array_push($fields, ['name' => 'state', 'value' => [], 'formatted_name' => 'State']);
                array_push($fields, ['name' => 'city', 'value' => [], 'formatted_name' => 'City']);
            } else {
                foreach ($this->getTableFields($request->table_name) as $field_name) {
                    array_push($fields, ['name' => $field_name, 'value' => '', 'formatted_name' => $this->dataStandardisation($field_name)]);
                }
            }

            array_push($fields, ['name' => 'dataAges', 'value' => [], 'formatted_name' => 'Data Age']);
        }


        return response()->json([
            'fields' => $fields
        ]);
    }

    public function getPriceRange(Request $request)
    {
        $price_0_to_7 = Setting::whereName('price_0_to_7')->first();
        $price_7_to_14 = Setting::whereName('price_7_to_14')->first();
        $price_14_to_30 = Setting::whereName('price_14_to_30')->first();
        $price_greater_30 = Setting::whereName('price_greater_30')->first();

        return response()->json([
            'price_0_to_7' => (int)$price_0_to_7->value,
            'price_7_to_14' => (int)$price_7_to_14->value,
            'price_14_to_30' => (int)$price_14_to_30->value,
            'price_greater_30' => (int)$price_greater_30->value
        ]);
    }

    public function search(Request $request)
    {
        $table = request('table');
        if ($this->getModelName($table) == "Mortgage" || $this->getModelName($table) == "Insurance") {
            $model = "\\App\\" . $this->getModelName($table);
        } else {
            $model = "\\App\\DataModels\\" . $this->getModelName($table);
        }

        $records = $model::with('user');

        $vendor_ids = [];
        if ($request->has('users')) {
            foreach ($request->users as $user) {
                $vendor_ids[] = (int)$user['value'];
            }

            $records->whereIn('user_id', $vendor_ids);
        }

        $suppression_foreign_key = str_singular($table) . '_id';

        $suppression_ids = DB::table($table . '_suppression')->where('user_id', auth()->id())->pluck($suppression_foreign_key);

        $records->whereNotIn('id', $suppression_ids);

        $all_vendor_ids = array_unique($model::pluck('user_id')->toArray());
        $vendors = User::whereIn('id', $all_vendor_ids)->get(['name', 'id']);
        $names = User::whereIn('id', $vendor_ids)->pluck('name');

        $data_ages = [];

        if ($request->has('queries')) {
            foreach ($request->queries as $query) {
                if (isset($query['value'])) {
                    if (is_array($query['value'])) {
                        if ($query['name'] == 'dataAges') {
                            $data_ages = $query['value'];
                        } else {
                            $records->whereIn($query['name'], collect($query['value'])->pluck('value'));
                        }
                    } elseif ($query['value'] != '') {
                        if ($query['name'] == 'amount') {
                            if ($query['name'] == 'greater_200000') {
                                $records->where('amount', '>', 200000);
                            } else {
                                $records->whereBetween('amount', [0, $query['value']]);
                            }
                        } else {
                            $records->where($query['name'], 'like', '%' . $query['value'] . '%');
                        }
                    }
                }
            }
        }

        if ($request->has('zip_code') && $request->has('radius')) {
            $codes = ZipCode::radiusSearch($request->zip_code, $request->radius, 0, "miles");

            $fields = $this->getTableFields($table);

            foreach ($fields as $field) {
                if (str_contains($field, 'post') || str_contains($field, 'code') || str_contains($field, 'zip')) {
                    $records->whereIn($field, $codes);
                }
            }

        }

        // Filter data by age ranges
        $range1_counts = [];
        $range2_counts = [];
        $range3_counts = [];
        $range4_counts = [];

        $record_ids = [];

        if (count($data_ages)) {
            $dataAges = $data_ages;

            if (in_array("7", $dataAges)) {
                $range1 = clone $records;
                $range1 = $range1->where('created_at', '>=', Carbon::now()->subDays(7))->get();
                $record_ids = array_collapse([$record_ids, $range1->pluck('id')->toArray()]);
                $range1 = $range1->groupBy('user_id');
                foreach ($range1 as $id => $data) {
                    array_push($range1_counts, count($data));

                    session(['order_details.' . $id => ['range1_count' => count($data)]]);
                }
            }

            if (in_array("14", $dataAges)) {
                $range2 = clone $records;
                $range2 = $range2->where([
                    ['created_at', '>=', Carbon::now()->subDays(14)],
                    ['created_at', '<=', Carbon::now()->subDays(7)]
                ])->get();
                $record_ids = array_collapse([$record_ids, $range2->pluck('id')->toArray()]);
                $range2 = $range2->groupBy('user_id');
                foreach ($range2 as $id => $data) {
                    array_push($range2_counts, count($data));

                    session(['order_details.' . $id => ['range2_count' => count($data)]]);
                }
            }

            if (in_array("30", $dataAges)) {
                $range3 = clone $records;

                $range3 = $range3->where('created_at', '>=', Carbon::now()->subDays(30))->where('created_at', '<=', Carbon::now()->subDays(14))->get();
                $record_ids = array_collapse([$record_ids, $range3->pluck('id')->toArray()]);
                $range3 = $range3->groupBy('user_id');
                foreach ($range3 as $id => $data) {
                    array_push($range3_counts, count($data));

                    session(['order_details.' . $id => ['range3_count' => count($data)]]);
                }
            }

            if (in_array('30+', $dataAges)) {
                $range4 = clone $records;

                $range4 = $range4->where('created_at', '<=', Carbon::now()->subDays(30))->get();
                $record_ids = array_collapse([$record_ids, $range4->pluck('id')->toArray()]);
                $range4 = $range4->groupBy('user_id');

                foreach ($range4 as $id => $data) {
                    array_push($range4_counts, count($data));

                    session(['order_details.' . $id => ['range4_count' => count($data)]]);
                }
            }

        } else {

            $range1 = clone $records;
            $range2 = clone $records;
            $range3 = clone $records;
            $range4 = clone $records;

            $range1 = $range1->where('created_at', '>=', Carbon::now()->subDays(7))->get();
            $record_ids = array_collapse([$record_ids, $range1->pluck('id')->toArray()]);
            $range1 = $range1->groupBy('user_id');

            $range2 = $range2->where([
                ['created_at', '>=', Carbon::now()->subDays(14)],
                ['created_at', '<=', Carbon::now()->subDays(7)]
            ])->get();

            $record_ids = array_collapse([$record_ids, $range2->pluck('id')->toArray()]);
            $range2 = $range2->groupBy('user_id');

            $range3 = $range3->where('created_at', '>=', Carbon::now()->subDays(30))->where('created_at', '<=', Carbon::now()->subDays(14))->get();
            $record_ids = array_collapse([$record_ids, $range3->pluck('id')->toArray()]);
            $range3 = $range3->groupBy('user_id');

            $range4 = $range4->where('created_at', '<=', Carbon::now()->subDays(30))->get();
            $record_ids = array_collapse([$record_ids, $range4->pluck('id')->toArray()]);
            $range4 = $range4->groupBy('user_id');

            foreach ($range1 as $id => $data) {
                array_push($range1_counts, count($data));

                session(['order_details.' . $id => ['range1_count' => count($data)]]);
            }

            foreach ($range2 as $id => $data) {
                array_push($range2_counts, count($data));

                session(['order_details.' . $id => ['range2_count' => count($data)]]);
            }

            foreach ($range3 as $id => $data) {
                array_push($range3_counts, count($data));

                session(['order_details.' . $id => ['range3_count' => count($data)]]);
            }

            foreach ($range4 as $id => $data) {
                array_push($range4_counts, count($data));

                session(['order_details.' . $id => ['range4_count' => count($data)]]);
            }
        }

        return response()->json([
            'records_count' => array_sum($range1_counts) + array_sum($range2_counts) + array_sum($range3_counts) + array_sum($range4_counts),
            'records' => ['range1' => $range1_counts, 'range2' => $range2_counts, 'range3' => $range3_counts, 'range4' => $range4_counts],
            'options' => $vendors,
            'record_ids' => $record_ids,
            'names' => $names,
            'queries' => $request->queries
        ]);

    }

    public function getVendors()
    {
        $table = request('table');
        if ($this->getModelName($table) == "Mortgage" || $this->getModelName($table) == "Insurance") {
            $model = "\\App\\" . $this->getModelName($table);
        } else {
            $model = "\\App\\DataModels\\" . $this->getModelName($table);
        }

        $user_ids = $model::distinct('user_id')->pluck('user_id');

        $vendors = User::whereIn('id', $user_ids)->get(['name', 'id']);

        return response()->json(['options' => $vendors,
            'stateOptions' => array_values(getUsaStates()),
            'cityOptions' => array_flatten(array_values(getUsaCities()))
        ]);
    }

    public function sidebarSettings()
    {
        session([auth()->id() . '_collapse' => request('value')]);

        return response()->json(['value' => session(auth()->id() . '_collapse')]);
    }

    public function getVendorUsers()
    {
        $vendors = Role::findByName('vendor')->users;

        return response()->json(['vendors' => $vendors]);
    }

    public function updateVendor(Request $request, $id)
    {

        $validator = Validator::make($request->all(), ['name' => 'required']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 422);
        }

        $foundEmail = User::where('id', '!=', $id)->where('email', $request->email)->count();

        if (!$foundEmail) {
            $user = User::find($id);

            $user->update($request->all());

            return $request->all();

        } else {
            return response()->json(['errors' => ['The email field has already been taken.']], 422);
        }
    }

    public function inviteBuyer(Request $request)
    {
        $validator = Validator::make($request->all(), ['email' => 'email|required']);

        if ($validator->fails()){
            return response()->json(["errors" => $validator->errors()->all()],422);
        }

        $email = $request->email;
        $token = str_random(40);
        if ($email) {
            Invitation::insert([
                'user_id' => auth()->id(),
                'email' => $email,
                'token' => $token,
                'id' => str_random(20),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            $url = url('register/?userId=' . auth()->id() . '&token=' . $token);
            Mail::to($email)->send(new InviteBuyer($url));
        }
    }
}
