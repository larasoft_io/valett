<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{

    /**
     * Handle an incoming request.
     * @param $request
     * @param Closure $next
     * @param $roles
     * @param null $permission
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle($request, Closure $next , $roles, $permission = null)
    {
        if (auth()->guest()) {
            return redirect('/login');
        }

        $roles = explode('|', $roles);

        $allow_count = 0;

        foreach ($roles as $role){
            $allow_count += $request->user()->hasRole($role) ? 1 : 0;
        }

        if (! $allow_count) {
            return redirect()->back();
        }

        if (isset($permission) && ! $request->user()->can($permission)) {
            return redirect()->back();
        }

        return $next($request);

    }
}
