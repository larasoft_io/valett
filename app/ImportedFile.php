<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportedFile extends Model
{
    protected $fillable = ['user_id', 'file_name' , 'file_extension' , 'file_size'];}
