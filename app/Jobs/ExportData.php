<?php

namespace App\Jobs;

use App\Mail\BuyerBuildComplete;
use App\Notifications\NotifyBuyerBuildComplete;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;


class ExportData implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $table_ids_from_suppression = DB::table($this->data['table_name'] . '_suppression')
            ->where('user_id', $this->data['user_id'])
            ->whereIn(str_singular($this->data['table_name']) . '_id', $this->data['record_ids'])
            ->pluck(str_singular($this->data['table_name']) . '_id');

        $records = DB::table($this->data['table_name'])
            ->whereIn('id', $this->data['record_ids'])
            ->whereNotIn('id', $table_ids_from_suppression)
            ->get($this->data['field_names']);

        $data = [];
        foreach ($records as $record) {
            $data[] = (array)$record;
        }

        if (count($data)) {
            Excel::create($this->data['file_name'], function ($excel) use ($data) {

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->fromArray($data);
                });

            })->store($this->data['file_format'], storage_path('purchased_files'));
        }

        $user = User::find($this->data['user_id']);

        Mail::to($user->email)->send(new BuyerBuildComplete($this->data['url']));

        $data = [
            'url' => $this->data['url'],
            'message' => 'Hi ' . $this->data['user_name'] . ', your file is ready for download.'
        ];

        $user->notify(new NotifyBuyerBuildComplete($data));
    }
}
