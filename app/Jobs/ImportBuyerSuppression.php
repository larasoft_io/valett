<?php

namespace App\Jobs;

use App\SuppressionInformation;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ImportBuyerSuppression implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $table = $this->data['table'];
        $file = $this->data['file'];
        $user_id = $this->data['user_id'];
        $csv_field = $this->data['csv_field'];
        $table_fields = $this->data['table_fields'];
        $original_file_name = $this->data['file_original_name'];

        Excel::load($file, function ($reader) use ($table, $user_id, $csv_field, $table_fields,$original_file_name) {
            $results = $reader->get();

            if (!empty($results)) {
                $count = 0;
                foreach ($results as $row) {

                    $records = DB::table($table);

                    foreach ($table_fields as $index => $table_field) {
                        if($index == 0) {
                            $records = $records->where($table_field, $row->$csv_field);
                        }else{
                            $records = $records->orWhere($table_field, $row->$csv_field);
                        }
                    }

                    $record = $records->first();

                    if ($record){
                        $data['created_at'] = $data['updated_at'] = Carbon::now();
                        $data['user_id'] = $user_id;
                        $data[$table.'_id'] = $record->id;

                        $already_in_suppression = DB::table($table."_suppression")->where([
                            ['user_id', '=', $user_id],
                            [$table.'_id', '=', $record->id]
                        ])->count();

                        if (!$already_in_suppression) {
                            DB::table($table . "_suppression")->insert($data);
                            $count++;

                        }
                    }
                }

                if($count > 0) {
                    SuppressionInformation::create([
                        'user_id' => $user_id,
                        'file_name' => $original_file_name,
                        'table_name' => $table,
                        'no_of_records' => $count
                    ]);
                }
            }
        });

        File::delete($file);
    }
}
