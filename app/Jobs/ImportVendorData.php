<?php

namespace App\Jobs;

use App\RedundantFile;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\ImportCsvTrait;


class ImportVendorData implements ShouldQueue
{
    use ImportCsvTrait;
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = $this->data['file_name'];
        $csv_fields = $this->data['csv_fields'];
        $table_fields = $this->data['table_fields'];
        $user_id = $this->data['user_id'];
        $table_name = $this->data['table_name'];

        Excel::load('storage/csv/'.$file, function ($reader) use ($csv_fields, $table_fields, $user_id, $table_name) {
            $results = $reader->get();

            if (!empty($results)) {
                if ($this->getModelName($table_name) == "Mortgage" || $this->getModelName($table_name) == "Insurance"){
                    $model = "\\App\\" . $this->getModelName($table_name);
                }
                else {
                    $model = "\\App\\DataModels\\" . $this->getModelName($table_name);
                }

                foreach ($results as $row) {
                    $data = [];

                    foreach ($csv_fields as $index => $field) {
                        if ($table_fields[$index] != '') {
                            $data[$table_fields[$index]] = $this->dataStandardisation($row->$field);
                        }
                    }

                    $data['user_id'] = $user_id;
                    $data['created_at'] = $data['updated_at'] = Carbon::now();

                    $model::insert($data);
                }
            }

        });

//        $file_name = session('step1.filename');
//        $file = storage_path() . '/csv/' .$file_name ;
//
//        if(file_exists($file)){
//            File::delete($file);
//            $get_file_row = RedundantFile::where('file_name', $file_name)->first();
//            $get_file_row->delete();
//        }

    }
}
