<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'email', 'email_cost','selected_columns','selected_unavailable_columns','unavailable_columns_cost','file_format','card_last_four','stripe_customer_id','table_name','table_ids','file_path', 'records_count', 'total_price', 'total', 'is_email'];

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    // Accessors
    public function getFileNameAttribute(){
        return $this->file_path;
    }

    public function getDownloadUrlAttribute(){
        return url("/orders/{$this->id}/file/download");
    }

    public function getPurchasedDateAttribute(){
        return url("/orders/{$this->id}/file/download");
    }

    public function getIsExpiredAttribute(){
        return url("/orders/{$this->id}/file/download");
    }
}
