<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['user_id', 'order_id', 'no_of_records', 'total'];

    public function order(){
        return $this->belongsTo(Order::class)->with('user');
    }

}
