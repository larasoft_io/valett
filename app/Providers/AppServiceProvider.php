<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;
use App\Traits\ImportCsvTrait;
class AppServiceProvider extends ServiceProvider
{
    use ImportCsvTrait;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('DB_LIVE'))
        {
            $price_0_to_7 = Setting::where('name', 'price_0_to_7')->first();
            $price_7_to_14 = Setting::where('name', 'price_7_to_14')->first();
            $price_14_to_30 = Setting::where('name', 'price_14_to_30')->first();
            $price_greater_30 = Setting::where('name', 'price_greater_30')->first();

            view()->share('price_0_to_7', $price_0_to_7->value);
            view()->share('price_7_to_14', $price_7_to_14->value);
            view()->share('price_14_to_30', $price_14_to_30->value);
            view()->share('price_greater_30', $price_greater_30->value);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
