<?php

namespace App\Providers;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Laravel\Spark\Invitation;
use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Your Company',
        'product' => 'Your Product',
        'street' => 'PO Box 111',
        'location' => 'Your Town, NY 12345',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'vendor@valett.com',
        'admin@valett.com',
//        'reseller@valett.com'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Spark::useStripe()->noCardUpFront()->trialDays(10);

        Spark::freePlan()
            ->features([
                'First', 'Second', 'Third'
            ]);

        Spark::plan('Basic', 'basic')
            ->price(10)
            ->features([
                'First', 'Second', 'Third'
            ]);


        Spark::createUsersWith(function ($request) {

            $data = $request->all();

             Log::info($data);
            if($request->has('token'))
            {
               $invitation = Invitation::whereNull('team_id')->where('token', $request->token)->first();
                if($invitation){
                    $userData = [
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'password' => bcrypt($data['password']),
                        'reseller_id' => $invitation->user_id,
                        'last_read_announcements_at' => Carbon::now(),
                        'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
                    ];
                    $invitation->delete();
                }
            }
            else {

                $userData = [
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'last_read_announcements_at' => Carbon::now(),
                    'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
                ];

            }

            $user = Spark::user($userData);

            $data = $request->all();

            $user->forceFill($userData)->save();

            $user->assignRole('buyer');

            return $user;
        });
    }
}
