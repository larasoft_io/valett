<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedundantFile extends Model
{
    protected $fillable = ['user_id', 'file_name'];
}
