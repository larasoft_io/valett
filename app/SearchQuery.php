<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchQuery extends Model
{
    protected $fillable = ['user_id','name','table_name','vendor_ids','fields'];

}
