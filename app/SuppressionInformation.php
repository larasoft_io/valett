<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppressionInformation extends Model
{

    protected $table = 'suppression_information';

    protected $fillable = ['user_id', 'file_name' , 'table_name' , 'no_of_records'];
}
