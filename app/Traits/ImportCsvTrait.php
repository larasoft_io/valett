<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

trait ImportCsvTrait {

    /**
     * @return array
     */
    public function getDBTables($user = null)
    {
        $db_tables = DB::select('SHOW TABLES');

        $property = "Tables_in_" . env('DB_DATABASE');

        $tables = array_map(create_function('$o', 'return $o->' . $property . ';'), $db_tables);

        $tables = array_values(array_diff($tables, [
            "announcements",
            "api_tokens",
            "invitations",
            "invoices",
            "migrations",
            "notifications",
            "password_resets",
            "performance_indicators",
            "permissions",
            "role_has_permissions",
            "roles",
            "subscriptions",
            "team_subscriptions",
            "team_users",
            "teams",
            "user_has_permissions",
            "user_has_roles",
            "users",
            "jobs",
            'settings',
            'search_queries',
            'redundant_files',
            'imported_files',
            'orders',
            'order_details',
            'zip_codes',
            'suppression_information'
        ]));

        if ($user) {
            foreach ($tables as $key => $one) {
                if ($user == 'suppression'){    // Return only suppression tables
                    if (strpos($one, '_suppression') == false)
                        unset($tables[$key]);
                }else{
                    if (!$user->hasRole('admin')) {
                        if (strpos($one, '_suppression') !== false)
                            unset($tables[$key]);
                    }
                }
            }
        }

        return array_combine($tables, $tables);
    }

    function removeSuppressionFilter($string) {
        return strpos($string, '_suppression') === false;
    }

    /**
     * @return array
     */
    public function getCombinedTableFields($tableName)
    {
        $columns = array_values(array_diff(Schema::getColumnListing($tableName), ['id', 'user_id', 'created_at', 'updated_at']));
        return array_combine($columns, $columns);
    }

    public function getTableFields($tableName){
        $columns = array_values(array_diff(Schema::getColumnListing($tableName), ['id', 'user_id', 'created_at', 'updated_at']));
        return $columns;
    }

    public function getAllTableFields($tableName)
    {
        return Schema::getColumnListing($tableName);
    }

    public function dataStandardisation($string)
    {
        $found = strpos($string, '@');
        if(!$found){
            $string = str_replace('_', ' ', preg_replace('/[^\w ]+/', '', $string));
            $formattedString = ucwords($string);

            return $formattedString;

        }else{
            return $string;
        }
    }

    function getModelName($table_name){
        $model = str_singular(ucfirst(camel_case($table_name)));

        if ($model == "Parent"){
            $model = "Parents";
        }

        return $model;
    }
}