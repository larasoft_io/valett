<?php

namespace App;

use App\Traits\ImportCsvTrait;
use Illuminate\Notifications\Notifiable;
use Laravel\Spark\Notification;
use Laravel\Spark\User as SparkUser;
use Spatie\Permission\Traits\HasRoles;


class User extends SparkUser
{

    use HasRoles, ImportCsvTrait, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'price_0_to_7_days_old',
        'price_7_to_14_days_old',
        'price_14_to_30_days_old',
        'price_greater_than_30_days_old',
        'revenue_percentage'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    public function searchQueries(){
        return $this->hasMany(SearchQuery::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function orderByVendors(){
        return $this->hasMany(OrderDetail::class)->with('order');
    }

    public function suppressionInformation(){
        return $this->hasMany(SuppressionInformation::class);
    }

    // Get invited buyers (clients) of Reseller
    public function clients(){
        return $this->hasMany(User::class, 'reseller_id');
    }

    /**
     * Get the entity's notifications.
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class)
            ->orderBy('created_at', 'desc');
    }
}
