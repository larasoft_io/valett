<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

class ZipCode extends Model
{
    const MILES = 3958.756;
    const FEET = 20902253;
    const METERS = 6371000;
    const KILOMETERS = 6371;
    const DEGREES = 57.2957795;
    const RADIANS = 1;

    public static function findByZipCode($zipCode) {

        $record = self::where('zip_code', '=', $zipCode)->first();

        if ( is_null($record) )
            return null;

        return $record;

    }

    public static function radiusSearch($zipCode, $outerRadius, $innerRadius=0, $unit=null) {

        $zipcode = self::findByZipCode($zipCode);

        if (isset($zipcode)) {
            $latitude = $zipcode->latitude;
            $longitude = $zipcode->longitude;

            if ($unit == 'miles') {
                $unit = self::MILES;
            }

            // MySQL will cache the results of the distance function during the connection
            // After the connection closes, Laravel will rember this forever.
            return self::selectRaw("*, WGS84distance($latitude, $longitude, `latitude`, `longitude`) * $unit AS distance")
                ->whereNotNull('latitude')
                ->whereNotNull('longitude')
                ->orderBy('distance', 'asc')
                ->get()->reject(function ($record, $key) use ($outerRadius, $innerRadius) {
                    return !($record->distance <= $outerRadius && $record->distance >= $innerRadius);
                })->pluck('zip_code');
        }else{
            return [];
        }

    }

}
