<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Mortgage::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'amount' => $faker->randomFloat(2, 100, 100000),
        'address' => $faker->address,
        'created_at' => $faker->dateTimeBetween('-40 days', 'now')
    ];
});

$factory->define(App\Insurance::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'plan' => $faker->numberBetween(0, 1) ? 'Premium' : 'Basic',
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->state,
        'zip_code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'expiry_date' => $faker->date('Y-m-d', '+30 years'),
        'created_at' => $faker->dateTimeBetween('-40 days', 'now')
    ];
});

