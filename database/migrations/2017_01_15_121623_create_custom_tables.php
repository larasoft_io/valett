<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Traits\ImportCsvTrait;

class CreateCustomTables extends Migration
{
    use ImportCsvTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = $this->getDBTables();

        foreach ($tables as $table) {
            Schema::dropIfExists($table.'_suppression');
            Schema::dropIfExists($table);
        }
    }
}
