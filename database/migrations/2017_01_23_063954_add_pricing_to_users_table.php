<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricingToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->decimal('price_0_to_7_days_old')->nullable();
            $table->decimal('price_7_to_14_days_old')->nullable();
            $table->decimal('price_14_to_30_days_old')->nullable();
            $table->decimal('price_greater_than_30_days_old')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('price_0_to_7_days_old');
            $table->dropColumn('price_7_to_14_days_old');
            $table->dropColumn('price_14_to_30_days_old');
            $table->dropColumn('price_greater_than_30_days_old');
        });
    }
}
