<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSearchQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('search_queries', function (Blueprint $table) {
            $table->string('name')->after('user_id');
            $table->string('table_name')->after('name');
            $table->string('vendor_ids')->after('table_name');
            $table->text('fields')->after('vendor_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('search_queries', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('table_name');
            $table->dropColumn('vendor_ids');
            $table->dropColumn('fields');
        });
    }
}
