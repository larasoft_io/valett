<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->decimal('email_cost')->nullable();
            $table->text('selected_columns')->nullable();
            $table->text('selected_unavailable_columns')->nullable();
            $table->decimal('unavailable_columns_cost')->nullable();
            $table->string('file_format');
            $table->integer('card_last_four')->nullable();
            $table->string('stripe_customer_id')->nullable();
            $table->string('table_name');
            $table->text('table_ids')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
