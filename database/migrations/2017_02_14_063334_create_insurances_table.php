<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();     // Vendor ID
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('name')->nullable();
            $table->string('plan')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('expiry_date')->nullable();
            $table->timestamps();
        });

        Schema::create('insurances_suppression', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();     // Vendor ID
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('insurance_id')->unsigned();
            $table->foreign('insurance_id')->references('id')->on('insurances')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances_suppression');
        Schema::dropIfExists('insurances');
    }
}
