<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsMortgageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mortgage', function (Blueprint $table) {
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('phone')->nullable();
            $table->string('income')->nullable();
            $table->string('home_value')->nullable();
            $table->string('interest_rate')->nullable();
            $table->string('behind')->nullable();
            $table->string('lender')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('ltv')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mortgage', function (Blueprint $table) {
            $table->dropColumn('first_name')->nullable();
            $table->dropColumn('last_name')->nullable();
            $table->dropColumn('city')->nullable();
            $table->dropColumn('state')->nullable();
            $table->dropColumn('zip')->nullable();
            $table->dropColumn('phone')->nullable();
            $table->dropColumn('income')->nullable();
            $table->dropColumn('home_value')->nullable();
            $table->dropColumn('interest_rate')->nullable();
            $table->dropColumn('behind')->nullable();
            $table->dropColumn('lender')->nullable();
            $table->dropColumn('ip_address')->nullable();
            $table->dropColumn('ltv')->nullable();
        });
    }
}
