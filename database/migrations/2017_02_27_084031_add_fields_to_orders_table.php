<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('is_email')->default(0)->after('email_cost');
            $table->string('records_count')->after('is_email');
            $table->decimal('total_price')->after('records_count');
            $table->decimal('total')->after('total_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('records_count');
            $table->dropColumn('is_email');
            $table->dropColumn('total_price');
            $table->dropColumn('total');
        });
    }
}
