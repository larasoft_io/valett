<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(SettingTableSeeder::class);
         $this->call(DemoDataSeeder::class);
         $this->call(ZipCodesTableSeeder::class);
         $this->call(MortgageDataTableSeeder::class);
    }
}
