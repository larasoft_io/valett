<?php

use Illuminate\Database\Seeder;
use \App\User;
use \App\Mortgage;
use \App\Insurance;
use \Illuminate\Support\Facades\Artisan;
use \Illuminate\Support\Facades\DB;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendor1 = User::where('email', 'vendor1@valett.com')->first();

        if ($vendor1)
        {
            factory(Mortgage::class, 6000)->create([
                'user_id' => $vendor1->id,
            ]);
        }

        $vendor2 = User::where('email', 'vendor2@valett.com')->first();

        if ($vendor2) {
            factory(Mortgage::class, 4000)->create([
                'user_id' => $vendor2->id,
            ]);
        }

        $vendor3 = User::where('email', 'vendor3@valett.com')->first();

        if ($vendor3) {
            factory(Insurance::class, 6000)->create([
                'user_id' => $vendor3->id,
            ]);
        }

        $vendor4 = User::where('email', 'vendor4@valett.com')->first();

        if ($vendor4) {
            factory(Insurance::class, 4000)->create([
                'user_id' => $vendor4->id,
            ]);
        }

        // Seed Suppression data for buyer1 and buyer2
        $buyer1 = User::where('email', 'buyer1@valett.com')->first();

        if ($buyer1){
            $mortgage_ids = Mortgage::get()->random(2000)->pluck('id');
            foreach ($mortgage_ids as $id) {
                $data = [];
                $data['created_at'] = $data['updated_at'] = \Carbon\Carbon::now();
                $data['user_id'] = $buyer1->id;
                $data['mortgage_id'] = $id;

                DB::table("mortgage_suppression")->insert($data);
            }
        }

        $buyer2 = User::where('email', 'buyer2@valett.com')->first();

        if ($buyer2){
            $insurance_ids = Insurance::get()->random(2000)->pluck('id');
            foreach ($insurance_ids as $id) {
                $data = [];
                $data['created_at'] = $data['updated_at'] = \Carbon\Carbon::now();
                $data['user_id'] = $buyer2->id;
                $data['insurance_id'] = $id;

                DB::table("insurances_suppression")->insert($data);
            }
        }


        $this->command->info('Demo data has been seeded.');

    }
}
