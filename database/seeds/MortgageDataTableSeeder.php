<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use App\Mortgage;
use \App\User;

class MortgageDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');

        $file = storage_path('seeds/mortgageData.xlsx');

        $vendor1 = User::where('email', 'vendor1@valett.com')->first();
        $vendor2 = User::where('email', 'vendor2@valett.com')->first();
        $user_ids=[];
        array_push($user_ids,$vendor1->id,$vendor2->id);

        if (File::exists($file)) {
            Excel::load($file, function($reader) use ($user_ids) {
                // Getting all results
                $results = $reader->get();
                foreach($results as $row){
                    Mortgage::create([
                        'user_id' => $user_ids[array_rand($user_ids)],
                        'first_name' => $row->first_name,
                        'last_name' => $row->last_name,
                        'email'   => $row->email,
                        'address' => $row->address,
                        'amount' => $row->amount,
                        'credit_rating' => $row->credit,
                        'city' => $row->city,
                        'state' => $row->state,
                        'zip' => $row->zip,
                        'phone' => $row->phone,
                        'income' => $row->income,
                        'home_value' => $row->home_value,
                        'interest_rate' => $row->interest_rate,
                        'behind' => $row->behind,
                        'lender' => $row->lender,
                        'ip_address' => $row->ip_address,
                        'ltv' => $row->ltv
                    ]);
                }

            });

        }
    }
}
