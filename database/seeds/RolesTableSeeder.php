<?php

use Illuminate\Database\Seeder;
use \Spatie\Permission\Models\Role;
use \Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Role::truncate();

        Role::insert([
            'name' => 'admin'
        ]);

        Role::insert([
            'name' => 'vendor'
        ]);

        Role::insert([
            'name' => 'reseller'
        ]);

        Role::insert([
            'name' => 'buyer'
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
