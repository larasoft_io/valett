<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = Setting::where('name', 'price_0_to_7')->first();
        if (!$setting) {

            $setting = Setting::create([
                'name' => 'price_0_to_7',
                'value' => 20

            ]);
        }

        $setting = Setting::where('name', 'price_7_to_14')->first();
        if (!$setting) {

            $setting = Setting::create([
                'name' => 'price_7_to_14',
                'value' => 15

            ]);
        }


        $setting = Setting::where('name', 'price_14_to_30')->first();
        if (!$setting) {

            $setting = Setting::create([
                'name' => 'price_14_to_30',
                'value' => 10

            ]);
        }


        $setting = Setting::where('name', 'price_greater_30')->first();
        if (!$setting) {

            $setting = Setting::create([
                'name' => 'price_greater_30',
                'value' => 5

            ]);
        }

    }
}
