<?php

use Illuminate\Database\Seeder;
use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed 1 Admin
        $user = User::where('email', 'admin@valett.com')->first();
        if (!$user) {

            $user = User::create([
                'name' => 'Admin',
                'email' => 'admin@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('admin');
        }

        // Seed 4 Vendors
        $user = User::where('email', 'vendor1@valett.com')->first();
        if (!$user) {

            $user = User::create([
                'name' => 'MortgageDataHouse',
                'email' => 'vendor1@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('vendor');
        }

        $user = User::where('email', 'vendor2@valett.com')->first();
        if (!$user) {

            $user = User::create([
                'name' => 'PrimeMortgageData',
                'email' => 'vendor2@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('vendor');
        }

        $user = User::where('email', 'vendor3@valett.com')->first();
        if (!$user) {

            $user = User::create([
                'name' => 'InsuranceData4You',
                'email' => 'vendor3@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('vendor');
        }

        $user = User::where('email', 'vendor4@valett.com')->first();
        if (!$user) {

            $user = User::create([
                'name' => 'BestInsuranceData',
                'email' => 'vendor4@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('vendor');
        }

        // Seed 1 Reseller
        $user = User::where('email', 'reseller@valett.com')->first();
        if (!$user) {
            $user = User::create([
                'name' => 'reseller',
                'email' => 'reseller@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('reseller');
        }

        // Seed 2 Buyers
        $user = User::where('email', 'buyer1@valett.com')->first();
        if (!$user) {
            $user = User::create([
                'name' => 'buyer1',
                'email' => 'buyer1@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('buyer');
        }

        $user = User::where('email', 'buyer2@valett.com')->first();
        if (!$user) {
            $user = User::create([
                'name' => 'buyer2',
                'email' => 'buyer2@valett.com',
                'password' => bcrypt('123456')
            ]);

            $user->assignRole('buyer');
        }
    }
}
