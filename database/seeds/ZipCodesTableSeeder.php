<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use App\ZipCode;

class ZipCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = storage_path('seeds/zip_codes_states.csv');
        if (File::exists($file)) {
            Excel::load($file, function($reader) {

                // Getting all results
                $results = $reader->get();
                 foreach($results as $row){
                     ZipCode::create([
                         'zip_code' => $row->zip_code,
                         'latitude' => $row->latitude,
                         'longitude' => $row->longitude
                     ]);
                 }

            });

        }

    }
}
