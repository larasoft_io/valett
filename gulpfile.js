var elixir = require('laravel-elixir');
var path = require('path');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.less('app.less')
        .webpack('app.js', null, null, {
            resolve: {
                modules: [
                    path.resolve(__dirname, 'vendor/laravel/spark/resources/assets/js'),
                    'node_modules'
                ]
            }
        })
        .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
        .copy('node_modules/sweetalert/dist/sweetalert.css', 'public/css/sweetalert.css');


    mix.styles([
        '/css/jasny-bootstrap.css',
        '/css/styles.css'

    ], 'public/css/main.css', 'resources/assets');

    mix.scripts([
        '/js/libs/jquery.bootstrap.wizard.js',
        '/js/libs/jasny-bootstrap.js',
        '/js/libs/jquery.validate.js',
        '/js/libs/additional-methods.min.js'

    ], 'public/js/main.js', 'resources/assets');


    mix.styles([
        <!-- Stylesheets -->
        '/global/css/bootstrap.css',
        '/global/css/bootstrap-extend.css',
        '/assets/css/site.css',
        '/global/vendor/select2/select2.css',
        '/global/vendor/blueimp-file-upload/jquery.fileupload.css',
        '/global/vendor/dropify/dropify.css',
        <!-- Plugins -->
        '/global/vendor/animsition/animsition.css',
        '/global/vendor/asscrollable/asScrollable.css',
        '/global/vendor/switchery/switchery.css',
        '/global/vendor/intro-js/introjs.css',
        '/global/vendor/slidepanel/slidePanel.css',
        '/global/vendor/flag-icon-css/flag-icon.css',
        <!-- Fonts -->
        '/global/fonts/web-icons/web-icons.min.css',
        '/global/fonts/themify/themify.css',
        '/global/fonts/brand-icons/brand-icons.min.css',
        "/global/vendor/chartist/chartist.css",
        "/assets/examples/css/widgets/chart.css",
        "/global/vendor/webui-popover/webui-popover.css",
        "/global/vendor/toolbar/toolbar.css",


    ], 'public/css/theme.css', 'resources/assets/theme');


    mix.scripts([
        '/global/vendor/babel-external-helpers/babel-external-helpers.js',
        '/global/vendor/tether/tether.js',
        // '/global/vendor/bootstrap/bootstrap.js',
        '/global/vendor/animsition/animsition.js',
        '/global/vendor/mousewheel/jquery.mousewheel.js',
        '/global/vendor/asscrollbar/jquery-asScrollbar.js',
        '/global/vendor/asscrollable/jquery-asScrollable.js',
        '/global/vendor/ashoverscroll/jquery-asHoverScroll.js',
        '/global/vendor/switchery/switchery.min.js',
        '/global/vendor/intro-js/intro.js',
        '/global/vendor/screenfull/screenfull.js',
        '/global/vendor/slidepanel/jquery-slidePanel.js',
        '/global/vendor/select2/select2.full.min.js',
        '/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js',
        '/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
        '/global/vendor/bootstrap-select/bootstrap-select.js',
        '/global/vendor/icheck/icheck.min.js',
        '/global/vendor/asrange/jquery-asRange.min.js',
        '/global/vendor/ionrangeslider/ion.rangeSlider.min.js',
        '/global/vendor/asspinner/jquery-asSpinner.min.js',
        '/global/vendor/clockpicker/bootstrap-clockpicker.min.js',
        '/global/vendor/ascolor/jquery-asColor.min.js',
        '/global/vendor/asgradient/jquery-asGradient.min.js',
        '/global/vendor/ascolorpicker/jquery-asColorPicker.min.js',
        '/global/vendor/bootstrap-maxlength/bootstrap-maxlength.js',
        '/global/vendor/jquery-knob/jquery.knob.js',
        '/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js',
        '/global/vendor/jquery-labelauty/jquery-labelauty.js',
        '/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js',
        '/global/vendor/jt-timepicker/jquery.timepicker.min.js',
        '/global/vendor/datepair/datepair.min.js',
        '/global/vendor/datepair/jquery.datepair.min.js',
        '/global/vendor/jquery-strength/password_strength.js',
        '/global/vendor/jquery-strength/password_strength.js',
        '/global/vendor/jquery-strength/jquery-strength.min.js',
        '/global/vendor/multi-select/jquery.multi-select.js',
        '/global/vendor/typeahead-js/bloodhound.min.js',
        '/global/vendor/typeahead-js/typeahead.jquery.min.js',
        '/global/vendor/jquery-placeholder/jquery.placeholder.js',
        '/global/js/State.js',
        '/global/js/Component.js',
        '/global/js/Plugin.js',
        '/global/js/Base.js',
        '/global/js/Config.js',
        '/assets/js/Section/Menubar.js',
        '/assets/js/Section/GridMenu.js',
        '/assets/js/Section/Sidebar.js',
        '/assets/js/Section/PageAside.js',
        '/assets/js/Plugin/menu.js',
        '/global/js/config/colors.js',
        '/assets/js/config/tour.js',
        '/assets/js/Site.js',
        '/global/js/Plugin/asscrollable.js',
        '/global/js/Plugin/slidepanel.js',
        '/global/js/Plugin/select2.js',
        '/global/js/Plugin/icheck.js',
        '/global/js/Plugin/asrange.js',
        '/global/js/Plugin/asspinner.js',
        '/global/js/Plugin/clockpicker.js',
        '/global/js/Plugin/ascolorpicker.js',
        '/global/js/Plugin/jquery-knob.js',
        '/global/js/Plugin/card.js',
        '/global/js/Plugin/jt-timepicker.js',
        '/global/js/Plugin/datepair.js',
        '/global/js/Plugin/jquery-strength.js',
        '/global/js/Plugin/multi-select.js',
        '/global/js/Plugin/jquery-placeholder.js',
        '/assets/examples/js/forms/advanced.js',
        "/global/vendor/chartist/chartist.js",
        // "/assets/examples/js/widgets/chart.js",
        "/global/vendor/webui-popover/jquery.webui-popover.min.js",
        "/assets/examples/js/uikit/tooltip-popover.js",
        "/global/vendor/datepair/jquery.datepair.min.js",
        "/global/vendor/jquery-strength/jquery-strength.min.js",
        "/global/vendor/multi-select/jquery.multi-select.js",
        "/global/vendor/typeahead-js/bloodhound.min.js",
        "/global/vendor/typeahead-js/typeahead.jquery.min.js",
        "/global/vendor/jquery-placeholder/jquery.placeholder.js",
        "/global/vendor/matchheight/jquery.matchHeight-min.js",
        "/global/js/State.js",
        "/global/js/Component.js",
        "/global/js/Plugin.js",
        "/global/js/Base.js",
        "/global/js/Config.js",
        "/assets/js/Section/Menubar.js",
        "/assets/js/Section/GridMenu.js",
        "/assets/js/Section/Sidebar.js",
        "/assets/js/Section/PageAside.js",
        "/assets/js/Plugin/menu.js",
        "/global/js/config/colors.js",
        "/assets/js/config/tour.js"
    ], 'public/js/theme.js', 'resources/assets/theme');


});
