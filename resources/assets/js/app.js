
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('spark-bootstrap');

require('./components/bootstrap');

import Multiselect from 'vue-multiselect'
import Vue from 'vue'
import VueRouter from 'vue-router'
import RiseLoader from 'vue-spinner/src/RiseLoader.vue'

// register globally
Vue.component('multiselect', Multiselect);
Vue.component('rise-loader', RiseLoader);

// Vue.component('example', require('./components/Example.vue'));
// import Example from "./components/Example.vue"


// import Components
import DashboardDefault from "./components/dashboard-default.vue"

import BuyerDashboard from "./components/buyer/search.vue"

import CheckoutStep1 from "./components/checkout/step1.vue"
import CheckoutStep2 from "./components/checkout/step2.vue"
import CheckoutStep3 from "./components/checkout/step3.vue"
import CheckoutStep4 from "./components/checkout/step4.vue"

import Orders from "./components/buyer/orders.vue"
import Invoice from "./components/buyer/invoice.vue"

import suppressionFiles from "./components/checkout/suppressionFiles.vue"

import Vendors from "./components/vendors/index.vue"
import orderByVendors from "./components/vendors/orders.vue"

import clients from "./components/reseller/clients.vue"

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: BuyerDashboard,
        meta: {
            role: 'buyer'
        }
    },
    {
        path: '/default',
        component: DashboardDefault
    },
    {
        path: '/checkout/step1',
        component: CheckoutStep1
    },
    {
        path: '/checkout/step2',
        component: CheckoutStep2
    },
    {
        path: '/checkout/step3',
        component: CheckoutStep3
    },
    {
        path: '/checkout/step4',
        component: CheckoutStep4
    },
    {
        path: '/orders',
        component: Orders
    },
    {
        path: '/invoices/:orderId',
        name: 'order',
        component: Invoice
    },
    {
        path: '/suppression/files',
        component: suppressionFiles
    },
    {
        path: '/vendor/orders',
        component: orderByVendors
    },
    {
        path: '/vendors',
        component: Vendors
    },
    {
        path: '/clients',
        component: clients
    }
];

const router = new VueRouter({
    routes // short for routes: routes
});

router.beforeEach((to, from, next) => {

    if(to.path == "/" && Spark.auth_role != ""){
        if(Spark.auth_role == 'buyer' || Spark.auth_role == 'reseller')
        {
            next();
        }else{
            next({
                path: '/default',
                query: { redirect: to.fullPath }
            });
        }
    }else{
        next();
    }
})

Spark.forms.register = {
    token: getQueryByName('token')
};

var app = new Vue({
    mixins: [require('spark')],
    router
});


