
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */

require('./../spark-components/bootstrap');

require('./home');

window.substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};

window.getQueryByName = function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

window.groupBy = function groupBy(arr, key) {
    var newArr = [],
        types = {},
        newItem, i, j, cur;
    for (i = 0, j = arr.length; i < j; i++) {
        cur = arr[i];
        if (!(cur[key] in types)) {
            types[cur[key]] = { type: cur[key], data: [] };
            newArr.push(types[cur[key]]);
        }
        types[cur[key]].data.push(cur);
    }
    return newArr;
};

window.filerDataByDateRange = function (data, min_date, max_date) {
    var dataToReturn = [];
    if (min_date != null) {
        $.each(data, function (index, object) {
            var date = moment(object.created_at, 'YYYY-MM-DD HH:mm:ss');
            if (date >= min_date && date <= max_date) {
                dataToReturn.push(object);
            }
        });
    }
    else{
        $.each(data, function (index, object) {
            var date = moment(object.created_at, 'YYYY-MM-DD HH:mm:ss');
            if (date <= max_date) {
                dataToReturn.push(object);
            }
        });
    }

    return dataToReturn;
};

window.filerDataByUserIds = function (data, user_ids) {
    var dataToReturn = [];

    $.each(data, function (index, object) {
        if (user_ids.indexOf(object.user_id) != -1) {
            dataToReturn.push(object);
        }
    });

    return dataToReturn;
};