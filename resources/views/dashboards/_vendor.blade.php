<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        @if(auth()->user()->hasRole('vendor'))
                            <a href="{!! route('vendor.import.data', 1) !!}" class="btn btn-primary"><i
                                        class="fa fa-file"></i> Import CSV Data</a>
                        @endif

                        {{--@if(auth()->user()->hasRole('buyer'))--}}
                        {{--<a href="{!! route('import.suppression', 1) !!}" class="btn btn-primary"><i--}}
                        {{--class="fa fa-file"></i> Import Suppression CSV File</a>--}}
                        {{--@endif--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>