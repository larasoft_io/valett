@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-12">
                <h1>Import CSV Data</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('partials._alerts')
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="rootwizard">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <ul class="nav nav-pills nav-justified thumbnail">
                                    <li class="active"><a href="#tab1" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 1</h4>
                                            <p class="list-group-item-text">Select CSV File & Table</p>
                                        </a></li>
                                    <li class="disabled"><a href="#tab2" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 2</h4>
                                            <p class="list-group-item-text">Map Fields</p>
                                        </a></li>
                                    <li class="disabled"><a href="#tab3" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 3</h4>
                                            <p class="list-group-item-text">Finish</p>
                                        </a></li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        <br>
                        <div class="tab-pane active" id="tab1">
                            {!! Form::open(['route' => 'vendor.import.step1', 'method' => 'post', 'files' => true , 'id' =>'step1Form']) !!}

                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">

                                    <div class="form-group">
                                        <label for="">Select CSV File*</label>
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"><i
                                                        class="glyphicon glyphicon-file fileinput-exists"></i> <span
                                                        class="fileinput-filename"></span></div>
                                            <span class="input-group-addon btn btn-default btn-file"><span
                                                        class="fileinput-new">Select file</span><span
                                                        class="fileinput-exists">Change</span>
                                                    <input type="file" name="file"></span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                        <div class="file-error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Select Type</label>
                                        {!! Form::select('table', [null => '-- Select --']+$tables , null , ['class' => 'form-control select-table']) !!}
                                    </div>
                                    <br>

                                    <div class="form-group">
                                        <div class="input-group">

                                              <span class="checkbox-custom checkbox-default">
                                                  {!! Form::checkbox('new_table', '1', null,  ['id' => 'create_table']) !!}
                                                <label for="inputCheckbox"> Create new Type?</label>
                                            </span>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="new-table"
                                         @if(!session()->has('new_table')) style="display: none" @endif>
                                        <div class="form-group">
                                            <label for="">Type Name:* </label>
                                            {!! Form::text('table_name', null, ['class' => 'form-control new_table_name']) !!}
                                        </div>
                                        <div id="errorMessage" class="alert alert-danger" style="display: none;"> This table already exist</div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary pull-right" type="submit"> Next
                                        <i class="fa fa-forward"></i>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="tab-pane" id="tab2">
                            <p>
                                <input type='text' name='name' id='name' placeholder='Enter Your Name'>
                            </p>
                        </div>
                        <div class="tab-pane" id="tab3">
                            3
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_styles')
    <style>
        #file-error
        {
            color: red;
        }
        #errorMessage
        {
            color: red;
        }

        #create_table
        {
            margin-top: 5px;
        }

        #rootwizard li.active h4{
            color: #fff;
        }
    </style>
@stop

@section('body_scripts')
    <script>

        var form = $( "form" );
        form.validate({
            rules: {
                file: {
                    required: true,
                    extension: "xls|csv"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "file" ) {
                    error.insertAfter(".file-error");
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#step1Form").submit(function (event) {
            if (form.valid()) {
                event.preventDefault();

                if ($('#create_table').is(':checked')) {
                    var table_name = $('.new_table_name').val();

                    $.ajax({
                        url: "{!! route('vendor.import.checkDatabase') !!}",
                        type: 'GET',
                        data: {table_name: table_name},
                        success: function (response) {
                            console.log(response);
                            if (response == 'success') {
                                $("#errorMessage").show();

                            } else {
                                $('#step1Form').unbind('submit').submit()
                            }
                        }
                    });
                } else {
                    $('#step1Form').unbind('submit').submit()

                }
            }
        });

        $('#create_table').change(function () {
            if ($(this).is(':checked')) {
                $('.select-table').prop('disabled', true);
                $('.new-table').slideDown();
            } else {
                $('.select-table').prop('disabled', false);
                $('.new-table').slideUp();
            }
        });



        $('#rootwizard').bootstrapWizard({
            onNext: function (tab, navigation, index) {
                if (index == 2) {
                    // Make sure we entered the name
                    if (!$('#name').val()) {
                        alert('You must enter your name');
                        $('#name').focus();
                        return false;
                    }
                }

                // Set the name for the next tab
                $('#tab3').html('Hello, ' + $('#name').val());

            }, onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#rootwizard .progress-bar').css({width: $percent + '%'});
            }
        });

    </script>
@stop