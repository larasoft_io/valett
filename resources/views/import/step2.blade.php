@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-12">
                <h1>Import CSV Data</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('partials._alerts')
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="rootwizard">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <ul class="nav nav-pills nav-justified thumbnail">
                                    <li class="disabled">
                                        <a href="#tab1" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 1</h4>
                                            <p class="list-group-item-text">Select CSV File & Table</p>
                                        </a>
                                    </li>
                                    <li class="active">
                                        <a href="#tab2" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 2</h4>
                                            <p class="list-group-item-text">Map Fields</p>
                                        </a>
                                    </li>
                                    <li class="disabled">
                                        <a href="#tab3" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 3</h4>
                                            <p class="list-group-item-text">Finish</p>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        <br>
                        {!! Form::open(['route' => 'vendor.import.step2', 'method' => 'post', 'files' => true]) !!}

                        <div class="tab-pane" id="tab2">
                            <div class="row">

                                <div class="col-md-12">

                                    @if(session('step1.new_table') != 1)

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span class="pull-left">Add New Columns</span>
                                                <button class="btn btn-success add-more pull-right" type="button">
                                                    <i class="fa fa-plus"></i> Add
                                                </button>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="panel-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group control-group after-add-more">
                                                            <input type="text" name="new_fields[]"
                                                                   class="form-control new-fields"
                                                                   placeholder="Enter Column Name">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-danger remove" type="button"><i
                                                                            class="fa fa-remove"></i></button>
                                                            </div>
                                                        </div>


                                                        <div class="copy hide">
                                                            <div class="control-group input-group" style="margin-top:10px">
                                                                <input type="text" name="new_fields[]"
                                                                       class="form-control new-fields"
                                                                       placeholder="Enter Column Name">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-danger remove" type="button"><i
                                                                                class="fa fa-remove"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endif

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>CSV Fields</span>
                                            <span class="pull-right">
                                                Table Fields
                                                @if(session('step1.new_table') != 1)
                                                <a href="#" onclick="refreshDropdowns()" class="btn btn-primary">
                                                    <i class="fa fa-refresh"></i></a>
                                                @endif
                                            </span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            <ul class="list-group">
                                                @foreach($csv_fields as $field)
                                                    <li class="list-group-item">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! $field !!}
                                                            </div>
                                                            @if(session('step1.new_table') != 1)
                                                                <div class="col-md-6">
                                                                    {!! Form::select('fields[]', [null => '-- IGNORE --']+$table_fields , null , ['class' => 'form-control mySelect']) !!}
                                                                </div>
                                                            @else

                                                                <div class="col-md-4">
                                                                    <input type="text" name="fields[]"
                                                                           placeholder="Enter name"
                                                                           class="form-control fields hidden">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button type="button" class="btn btn-success plus">
                                                                        <i class="fa fa-plus"></i></button>
                                                                    <button type="button" class="btn btn-danger minus">
                                                                        <i class="fa fa-minus"></i></button>
                                                                </div>
                                                            @endif

                                                        </div>

                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">

                                    <button class="btn btn-primary pull-right" type="submit"> Next
                                        <i class="fa fa-forward"></i>
                                    </button>

                                    <a style="margin-right: 5px" href="{!! route('vendor.import.data', 1) !!}"
                                       class="btn btn-default pull-right">
                                        <i class="fa fa-backward"></i> Previous
                                    </a>

                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_styles')
    <style>
        #file-error {
            color: red;
        }

        #rootwizard li.active h4{
            color: #fff;
        }
    </style>
@stop

@section('body_scripts')
    <script>

        $('.plus').click(function () {
            $(this).closest('.row').find('.fields').removeClass('hidden')
        });

        $('.minus').click(function () {
            $(this).closest('.row').find('.fields').addClass('hidden').val('')
        });

        $('#create_table').change(function () {
            if ($(this).is(':checked')) {
                $('.select-table').prop('disabled', true);
                $('.new-table').slideDown();
            } else {
                $('.select-table').prop('disabled', false);
                $('.new-table').slideUp();
            }
        });


        $(document).ready(function () {



//        $( "form" ).validate({
//                rules: {
//                    file: {
//                        required: true,
//                        extension: "xls|csv"
//                    }
//                },
//                errorPlacement: function(error, element) {
//                    if (element.attr("name") == "file" ) {
//                        error.insertAfter(".file-error");
//                    } else {
//                        error.insertAfter(element);
//                    }
//                }
//            });

            $('#rootwizard').bootstrapWizard({});
        });

        $(document).ready(function () {

            $(".add-more").click(function () {
                var html = $(".copy").html();
                $(".after-add-more").after(html);
            });

            $("body").on("click", ".remove", function () {
                $(this).parents(".control-group").remove();
            });

        });

        function refreshDropdowns() {
            $('.new-fields').each(function () {
                var value = $(this).val();

                if (value != '') {
                    $('.mySelect').append($('<option>', {
                        value: value,
                        text: value
                    }));
                }

                $(".mySelect option").each(function(){
                    $(this).siblings("[value='"+ this.value+"']").remove();
                });
            });

        }

        $('button.remove').click(function () {
            var value = $(this).closest('.control-group').find('.new-fields').val();
            console.log(value)
            $(".mySelect option").each(function(){
                $(this).siblings("[value='"+ value+"']").remove();
            });
        });

        {{--$( window ).unload(function() {--}}
            {{--$.ajax({--}}
                {{--url: "{!! route('file.redundancy') !!}",--}}
                {{--type: 'GET',--}}
                {{--data: '',--}}
                {{--success: function (response) {--}}
                    {{--console.log(response);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}


    </script>
@stop