@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-12">
                <h1>Import CSV Data</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{--@include('partials._alerts')--}}
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="rootwizard">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <ul class="nav nav-pills nav-justified thumbnail">
                                    <li class="disabled"><a href="#tab1" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 1</h4>
                                            <p class="list-group-item-text">Select CSV File & Table</p>
                                        </a></li>
                                    <li class="disabled"><a href="#tab2" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 2</h4>
                                            <p class="list-group-item-text">Map Fields</p>
                                        </a></li>
                                    <li class="active"><a href="#tab3" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 3</h4>
                                            <p class="list-group-item-text">Finish</p>
                                        </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        <br>
                        <div class="tab-pane active" id="tab3">
                            <div class="row">

                                <div class="col-md-12">
                                    @include('partials._alerts')
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <a style="margin-right: 5px" href="/dashboard" class="btn btn-default pull-right">
                                        <i class="fa fa-home"></i> Finish
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_styles')
    <style>
        #rootwizard li.active h4{
            color: #fff;
        }
    </style>
@stop

@section('body_scripts')
    <script>
        $(document).ready(function () {

            $('#rootwizard').bootstrapWizard({

            });
        });
    </script>

@stop