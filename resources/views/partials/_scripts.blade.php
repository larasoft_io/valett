
<script>
    Breakpoints();
</script>
<script>
    Config.set('assets', 'theme/assets');
</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);

    $('li.dropdown').click(function () {
        setTimeout(function () {
            $(this).toggleClass('open');
        }, 100)
    });

    $('#toggleMenubar a.nav-link').click(function () {

        var value = 1;

        if($('body').hasClass('site-menubar-fold')){
            value = 0;
            setTimeout(function () {
                unfold()
            }, 100)
        }else{
            setTimeout(function () {
                fold()
            }, 100)
        }

        $.ajax({
            url: "{!! route('sidebar.settings') !!}",
            type: 'POST',
            data: {value: value},
            success: function (response) {

            }
        });
    });

    setTimeout(function () {

        @if(session(auth()->id().'_collapse') == 1)
            fold();
        @else
            unfold();
        @endif

    }, 100);

    function fold() {
        $('body').addClass('site-menubar-fold');
        $('body').removeClass('site-menubar-unfold');
    }

    function unfold() {
        $('body').removeClass('site-menubar-fold');
        $('body').addClass('site-menubar-unfold');
    }

    // Active: /orders Sidebar Item
    if (window.location.href.indexOf("#/orders") > -1) {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.orders ').addClass('active');
    }
    else{
        $('.site-menubar .site-menu-item.orders ').removeClass('active');
    }

    $('.site-menubar .site-menu-item.orders ').click(function () {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.orders ').addClass('active');
    });

    // Active: suppression/files Sidebar Item
    if (window.location.href.indexOf("#/suppression/files") > -1) {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.files ').addClass('active');
    }
    else{
        $('.site-menubar .site-menu-item.files ').removeClass('active');
    }

    $('.site-menubar .site-menu-item.files ').click(function () {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.files ').addClass('active');
    });

    // Active: /payment-method Sidebar Item
    if (window.location.href.indexOf("#/payment-method") > -1) {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.payment ').addClass('active');
    }
    else{
        $('.site-menubar .site-menu-item.payment ').removeClass('active');
    }

    $('.site-menubar .site-menu-item.payment ').click(function () {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.payment ').addClass('active');
    });

    // Active: /vendor/orders Sidebar Item
    if (window.location.href.indexOf("#/vendor/orders") > -1) {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.vendorOrders ').addClass('active');
    }
    else{
        $('.site-menubar .site-menu-item.vendorOrders ').removeClass('active');
    }

    $('.site-menubar .site-menu-item.vendorOrders ').click(function () {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.vendorOrders ').addClass('active');
    });

    // Active: /vendor/orders Sidebar Item
    if (window.location.href.indexOf("#/vendors") > -1) {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.vendors ').addClass('active');
    }
    else{
        $('.site-menubar .site-menu-item.vendors ').removeClass('active');
    }

    $('.site-menubar .site-menu-item.vendors ').click(function () {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.vendors').addClass('active');
    });


    // Active: /reseller/clients Sidebar Item
    if (window.location.href.indexOf("#/clients") > -1) {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.client ').addClass('active');
    }
    else{
        $('.site-menubar .site-menu-item.client ').removeClass('active');
    }

    $('.site-menubar .site-menu-item.client ').click(function () {
        $('.site-menubar .site-menu-item').removeClass('active');
        $('.site-menubar .site-menu-item.client').addClass('active');
    });

    $("#target").val($("#target option:first").val());

    $(function () {
        setTimeout(function () {
            $(".select-field option:first").attr('selected','selected');
        }, 600);
    })

</script>