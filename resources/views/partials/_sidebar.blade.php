<div class="site-menubar">
    <div class="site-menubar-body scrollable scrollable-inverse is-enabled scrollable-vertical" style="position: relative;">
        <div class="scrollable-container" style="height: 1133px; width: 260px;">
            <div class="scrollable-content" style="width: 260px;">
                <ul class="site-menu">
                    <li class="site-menu-category"></li>
                    @if(auth()->user()->hasRole('buyer'))
                        <li class="site-menu-category">Search Data</li>
                        @foreach(auth()->user()->getDBTables(auth()->user()) as $table_name )
                            <li class="site-menu-item {!! request('table') == $table_name  ? 'active' : '' !!}">
                                <a href="/dashboard?table={{$table_name}}">
                                    <i class="site-menu-icon ti-package" aria-hidden="true"></i>
                                    <span class="site-menu-title">{!! ucfirst(camel_case($table_name)) !!}</span>
                                </a>
                            </li>
                        @endforeach
                        <li class="site-menu-category">Management</li>
                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!}">
                            <a href="{{ route('import.suppression', 1)  }}">
                                <i class="site-menu-icon wb-add-file" aria-hidden="true"></i>
                                <span class="site-menu-title">Manage Suppression</span>
                            </a>
                        </li>
                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} orders">
                            <a href="/dashboard/#/orders">
                                <i class="site-menu-icon wb-shopping-cart" aria-hidden="true"></i>
                                <span class="site-menu-title">Orders</span>
                            </a>
                        </li>

                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} files">
                            <a href="/dashboard/#/suppression/files">
                                <i class="site-menu-icon wb-add-file" aria-hidden="true"></i>
                                <span class="site-menu-title">Suppression Files</span>
                            </a>
                        </li>

                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} payment">
                            <a href="/settings#/payment-method">
                                <i class=" site-menu-icon fa fa-credit-card" aria-hidden="true"></i>
                                <span class="site-menu-title">Payment Methods</span>
                            </a>
                        </li>

                    @elseif (auth()->user()->hasRole('reseller'))
                        <li class="site-menu-category">Search Data</li>
                        @foreach(auth()->user()->getDBTables(auth()->user()) as $table_name )
                            <li class="site-menu-item {!! request('table') == $table_name  ? 'active' : '' !!}">
                                <a href="/dashboard?table={{$table_name}}">
                                    <i class="site-menu-icon ti-package" aria-hidden="true"></i>
                                    <span class="site-menu-title">{!! ucfirst(camel_case($table_name)) !!}</span>
                                </a>
                            </li>
                        @endforeach
                        <li class="site-menu-category">Management</li>
                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!}">
                            <a href="{{ route('import.suppression', 1)  }}">
                                <i class="site-menu-icon wb-add-file" aria-hidden="true"></i>
                                <span class="site-menu-title">Manage Suppression</span>
                            </a>
                        </li>
                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} orders">
                            <a href="/dashboard/#/orders">
                                <i class="site-menu-icon wb-shopping-cart" aria-hidden="true"></i>
                                <span class="site-menu-title">Orders</span>
                            </a>
                        </li>

                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} files">
                            <a href="/dashboard/#/suppression/files">
                                <i class="site-menu-icon wb-add-file" aria-hidden="true"></i>
                                <span class="site-menu-title">Suppression Files</span>
                            </a>
                        </li>

                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} payment">
                            <a href="/settings#/payment-method">
                                <i class=" site-menu-icon fa fa-credit-card" aria-hidden="true"></i>
                                <span class="site-menu-title">Payment Methods</span>
                            </a>
                        </li>

                        <li class="site-menu-item {!! request()->is('buyer/import/suppression*') ? 'active' : '' !!} client">
                            <a href="/dashboard/#/clients">
                                <i class=" site-menu-icon fa fa-users" aria-hidden="true"></i>
                                <span class="site-menu-title">Manage Users</span>
                            </a>
                        </li>
                    @else
                        <li class="site-menu-item {!! request()->is('dashboard*') ? 'active' : '' !!}">
                            <a href="/dashboard">
                                <i class="site-menu-icon ti-dashboard" aria-hidden="true"></i>
                                <span class="site-menu-title">Dashboard</span>
                            </a>
                        </li>
                        @if(auth()->user()->hasRole('vendor'))
                            <li class="site-menu-item {!! request()->is('vendor/import/data*') ? 'active' : '' !!}">
                                <a href="{!! route('vendor.import.data', 1) !!}">
                                    <i class="site-menu-icon ti-file" aria-hidden="true"></i>
                                    <span class="site-menu-title">Upload CSV File</span>
                                </a>
                            </li>

                            <li class="site-menu-item {!! request()->is('vendor/orders*') ? 'active' : '' !!} vendorOrders">
                            <a href="/dashboard/#/vendor/orders">
                            <i class="site-menu-icon fa fa-shopping-cart" aria-hidden="true"></i>
                            <span class="site-menu-title">Orders</span>
                            </a>
                            </li>
                        @endif


                        <li class="site-menu-item {!! request()->is('settings*') ? 'active' : '' !!}">
                            <a href="/settings">
                                <i class="site-menu-icon ti-user" aria-hidden="true"></i>
                                <span class="site-menu-title">Profile</span>
                            </a>
                        </li>



                        @if(auth()->user()->hasRole('admin'))
                            <li class="site-menu-category">Management</li>

                            <li class="site-menu-item {!! request()->is('vendors*') ? 'active' : '' !!} vendors">
                                <a href="/dashboard/#/vendors">
                                    <i class="site-menu-icon icon wb-users" aria-hidden="true"></i>
                                    <span class="site-menu-title">Vendors</span>
                                </a>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
        <div class="scrollable-bar scrollable-bar-vertical is-disabled scrollable-bar-hide" draggable="false"><div class="scrollable-bar-handle"></div></div></div>


    <div class="site-menubar-footer">
        <a href="/settings" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Settings">
            <span class="icon ti-settings" aria-hidden="true"></span>
        </a>
        {{--<a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">--}}
            {{--<span class="icon ti-lock" aria-hidden="true"></span>--}}
        {{--</a>--}}

        <a class="pull-right" href="/logout" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon ti-power-off" aria-hidden="true"></span>
        </a>
    </div>
</div>