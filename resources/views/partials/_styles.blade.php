<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

<style>


    @if(auth()->check())
        .navbar-nav > li > a, .navbar-brand, .hamburger {
            height: 17px;
        }

        .unread-notifications {
            color: #62a8ea !important;
        }
    @endif

    .navbar-inverse {
        border-color: rgba(0, 0, 0, .1);
        background-color: rgb(98, 180, 81);
    }

    .loader-content h2{
        text-transform: capitalize;
        font-size: 3em;
    }

    .loader-overlay{
        background-color: rgb(98, 180, 81);
    }

</style>