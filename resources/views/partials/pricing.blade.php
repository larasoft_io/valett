<div class="row">
    <!-- Monthly Recurring Revenue -->
    <form method="post" action="{!! route('settings.store') !!}">

        {{ csrf_field() }}

        <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading text-center">0 - 7 Days Old</div>

            <div class="panel-body text-center">
                <div  style="font-size: 12px; margin-top: 10px;">
                    <input type="text" name="price_0_to_7" value="{!! $price_0_to_7 !!}" class="form-control">
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading text-center">7 - 14 Days Old</div>

            <div class="panel-body text-center">
                <div  style="font-size: 12px; margin-top: 10px;">
                    <input type="text" name="price_7_to_14" value="{!! $price_7_to_14 !!}" class="form-control">
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading text-center">14 - 30 Days Old</div>
            <div class="panel-body text-center">
                <div  style="font-size: 12px; margin-top: 10px;">
                    <input type="text" name="price_14_to_30" value="{!! $price_14_to_30 !!}" class="form-control">
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading text-center">Greater than 30 Days Old</div>
            <div class="panel-body text-center">
                <div  style="font-size: 12px; margin-top: 10px;">
                    <input type="text" name="price_greater_30" value="{!! $price_greater_30 !!}" class="form-control">
                </div>


            </div>
        </div>
    </div>

    <div class="col-md-6">
   <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>
        Save</button>
        </div>
        </form>

</div>