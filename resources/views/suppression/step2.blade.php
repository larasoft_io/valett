@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-12">
                <h1>Import CSV Suppression</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('partials._alerts')
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="rootwizard">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <ul class="nav nav-pills nav-justified thumbnail">
                                    <li class="disabled"><a href="#tab1" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 1</h4>
                                            <p class="list-group-item-text">Select Suppression CSV File & Table</p>
                                        </a></li>
                                    <li class="active"><a href="#tab2" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 2</h4>
                                            <p class="list-group-item-text">Select Phone Number field</p>
                                        </a></li>
                                    <li class="disabled"><a href="#tab3" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 3</h4>
                                            <p class="list-group-item-text">Finish</p>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        <br>
                        {!! Form::open(['route' => 'suppression.step2', 'method' => 'post', 'files' => true, 'id' => 'form']) !!}

                        <div class="tab-pane active" id="tab2">
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span >De-duplicate Query</span>

                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <div class="form-group">
                                                        <label for="">Which field is the Phone Number stored in CSV?</label>
                                                        {!! Form::select('csv_field', [null => '-- Select --'] + array_combine($csv_fields, $csv_fields) , null , ['class' => 'form-control']) !!}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">

                                    <button class="btn btn-primary pull-right" type="submit"> Next
                                        <i class="fa fa-forward"></i>
                                    </button>

                                    <a style="margin-right: 5px" href="{!! route('import.suppression', 1) !!}" class="btn btn-default pull-right">
                                        <i class="fa fa-backward"></i> Previous
                                    </a>

                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_styles')
    <style>
        #file-error, .error
        {
            color: red;
        }

        #rootwizard li.active h4{
            color: #fff;
        }
    </style>
@stop

@section('body_scripts')
    <script>

        $('.plus').click(function () {
            $(this).closest('.row').find('.fields').removeClass('hidden')
        });

        $('.minus').click(function () {
            $(this).closest('.row').find('.fields').addClass('hidden').val('')
        });


        $('#create_table').change(function () {
            if ($(this).is(':checked')) {
                $('.select-table').prop('disabled', true);
                $('.new-table').slideDown();
            } else {
                $('.select-table').prop('disabled', false);
                $('.new-table').slideUp();
            }
        });

        $(document).ready(function () {

        $( "#form" ).validate({
                rules: {
                    csv_field: {
                        required: true
                    }
                }
            });

            $('#rootwizard').bootstrapWizard({

            });
        });
    </script>
@stop