@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-12">
                <h1>Import CSV Suppression</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="rootwizard">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <ul class="nav nav-pills nav-justified thumbnail">
                                    <li class="disabled">
                                        <a href="#tab1" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 1:</h4>
                                            <p class="list-group-item-text">Select Suppression CSV File & Table</p>
                                        </a>
                                    </li>
                                    <li class="disabled">
                                        <a href="#tab2" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 2:</h4>
                                            <p class="list-group-item-text">Select Phone Number field</p>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#tab3" data-toggle="tab">
                                            <h4 class="list-group-item-heading">Step 2</h4>
                                            <p class="list-group-item-text">Finish</p>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        <br>

                        <div class="tab-pane" id="tab1">

                        </div>
                        <div class="tab-pane" id="tab2">

                        </div>
                        <div class="tab-pane active" id="tab3">
                            @include('partials._alerts')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="/dashboard?table={!! isset($default_table) ? $default_table : '' !!}" class="btn btn-default pull-right"><i class="fa fa-home"></i> Finish</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_styles')
    <style>
        .error
        {
            color: red !important;
        }

        #rootwizard li.active h4{
            color: #fff;
        }
    </style>
@stop

@section('body_scripts')
    <script>
        $(document).ready(function () {
            $( "form" ).validate({
                rules: {
                    file: {
                        required: true,
                        extension: "xls|csv"
                    },
                    table: {
                        required: true,
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "file" ) {
                        error.insertAfter(".file-error");
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $('#rootwizard').bootstrapWizard({
                onNext: function (tab, navigation, index) {
                    if (index == 2) {
                        // Make sure we entered the name
                        if (!$('#name').val()) {
                            alert('You must enter your name');
                            $('#name').focus();
                            return false;
                        }
                    }

                    // Set the name for the next tab
                    $('#tab3').html('Hello, ' + $('#name').val());

                }, onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    $('#rootwizard .progress-bar').css({width: $percent + '%'});
                }
            });
        });
    </script>
@stop