@extends('spark::layouts.app')

@section('content')
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-8 col-md-offset-2">--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Login</div>--}}

    {{--<div class="panel-body">--}}
    {{--@include('spark::shared.errors')--}}

    {{--<form class="form-horizontal" role="form" method="POST" action="/login">--}}
    {{--{{ csrf_field() }}--}}

    {{--<!-- E-Mail Address -->--}}
    {{--<div class="form-group">--}}
    {{--<label class="col-md-4 control-label">E-Mail Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<!-- Password -->--}}
    {{--<div class="form-group">--}}
    {{--<label class="col-md-4 control-label">Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="password" class="form-control" name="password">--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<!-- Remember Me -->--}}
    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<div class="checkbox">--}}
    {{--<label>--}}
    {{--<input type="checkbox" name="remember"> Remember Me--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<!-- Login Button -->--}}
    {{--<div class="form-group">--}}
    {{--<div class="col-md-8 col-md-offset-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--<i class="fa m-r-xs fa-sign-in"></i>Login--}}
    {{--</button>--}}

    {{--<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    <div class="container">
        <div class="page-content text-xs-center">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="brand">
                        <img style="max-width: 100px; max-height: 100px;" class="brand-img" src="/img/lock.png" alt="...">
                        <h2 class="brand-text">Valett</h2>
                    </div>
                    <p>Sign into your account</p>
                    @include('partials._alerts')
                    <form method="post" action="/login">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="sr-only" for="inputEmail">Email</label>
                            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="inputPassword">Password</label>
                            <input type="password" class="form-control" id="inputPassword" name="password"
                                   placeholder="Password">
                        </div>
                        <div class="form-group clearfix">
                            <div class="checkbox-custom checkbox-inline checkbox-primary pull-xs-left">
                                <input type="checkbox" id="inputCheckbox" name="remember">
                                <label for="inputCheckbox">Remember me</label>
                            </div>
                            <a class="pull-xs-right" href="{{ url('/password/reset') }}">Forgot password?</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                    </form>
                </div>
            </div>

        </div>

    </div>
@endsection
