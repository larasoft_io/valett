<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name'))</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/theme.css" rel="stylesheet">

    @include('partials._styles')

    @yield('page_styles')

    <!-- Scripts -->

    @yield('scripts', '')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), [
                    'table_names' => auth()->check() ? auth()->user()->getDBTables(auth()->user()) : [],
                    'stripe_key' => config('services.stripe.key'),
                    'email_cost' => config('settings.email_cost'),
                    'auth_role' => auth()->check() ? auth()->user()->roles()->first()->name : ''
                ]
        )); ?>;
    </script>
</head>
<body class="with-navbar animsition">
    <div id="spark-app" v-cloak>
        <!-- Navigation -->
        @if (Auth::check())
            @include('spark::nav.user')

                @include('partials._sidebar')
        @else
            @include('spark::nav.guest')
        @endif

        @if (auth()->check())
            <!-- Page -->
            <div class="page">
            <div class="page-content">
            @endif
            <!-- Main Content -->
            @yield('content')

        @if (auth()->check())
            </div>
            </div>
        @endif

        <!-- Application Level Modals -->
        @if (Auth::check())
            @include('spark::modals.notifications')
            @include('spark::modals.support')
            @include('spark::modals.session-expired')
        @endif
    </div>

    <!-- JavaScript -->

    <script src="/theme/global/vendor/jquery/jquery.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/jquery.card.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/sweetalert.min.js"></script>

    <!--[if lt IE 9]>
    <script src="/theme/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="/theme/global/vendor/media-match/media.match.min.js"></script>
    <script src="/theme/global/vendor/respond/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="/theme/global/vendor/breakpoints/breakpoints.js"></script>

    <script src="/js/theme.js"></script>

    @include('partials._scripts')
    @yield('body_scripts')

</body>
</html>
