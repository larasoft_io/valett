{{--@if (Auth::user()->onTrial())--}}
    {{--<!-- Trial Reminder -->--}}
    {{--<li class="dropdown-header">Trial</li>--}}

    {{--<li>--}}
        {{--<a href="/settings#/subscription">--}}
            {{--<i class="fa fa-fw fa-btn fa-shopping-bag"></i>Subscribe--}}
        {{--</a>--}}
    {{--</li>--}}

    {{--<li class="divider"></li>--}}
{{--@endif--}}

{{--@if (Spark::usesTeams() && Auth::user()->currentTeamOnTrial())--}}
    {{--<!-- Team Trial Reminder -->--}}
    {{--<li class="dropdown-header">{{ ucfirst(Spark::teamString()) }} Trial</li>--}}

    {{--<li>--}}
        {{--<a href="/settings/{{ str_plural(Spark::teamString()) }}/{{ Auth::user()->currentTeam()->id }}#/subscription">--}}
            {{--<i class="fa fa-fw fa-btn fa-shopping-bag"></i>Subscribe--}}
        {{--</a>--}}
    {{--</li>--}}

    {{--<li class="divider"></li>--}}
{{--@endif--}}


@if (Auth::user()->onTrial())
    <!-- Trial Reminder -->
    <a class="dropdown-item dropdown-header" href="javascript:void(0)" role="menuitem">Trial</a>

        <a class="dropdown-item" role="menuitem" href="/settings#/subscription">
            <i class="fa fa-fw fa-btn fa-shopping-bag"></i>Subscribe
        </a>
@endif

@if (Spark::usesTeams() && Auth::user()->currentTeamOnTrial())
    <!-- Team Trial Reminder -->
    <a class="dropdown-item dropdown-header" href="javascript:void(0)" role="menuitem">{{ ucfirst(Spark::teamString()) }} Trial</a>

        <a class="dropdown-item" role="menuitem" href="/settings/{{ str_plural(Spark::teamString()) }}/{{ Auth::user()->currentTeam()->id }}#/subscription">
            <i class="fa fa-fw fa-btn fa-shopping-bag"></i>Subscribe
        </a>

@endif
