{{--<!-- Teams -->--}}
{{--<li class="dropdown-header">{{ ucfirst(str_plural(Spark::teamString())) }}</li>--}}

{{--<!-- Create Team -->--}}
{{--@if (Spark::createsAdditionalTeams())--}}
    {{--<li>--}}
        {{--<a href="/settings#/{{str_plural(Spark::teamString())}}">--}}
            {{--<i class="fa fa-fw fa-btn fa-plus"></i>Create {{ ucfirst(Spark::teamString()) }}--}}
        {{--</a>--}}
    {{--</li>--}}
{{--@endif--}}

{{--<!-- Switch Current Team -->--}}
{{--@if (Spark::showsTeamSwitcher())--}}
    {{--<li v-for="team in teams">--}}
        {{--<a :href="'/{{ str_plural(Spark::teamString()) }}/'+ team.id +'/switch'">--}}
            {{--<span v-if="user.current_team_id == team.id">--}}
                {{--<i class="fa fa-fw fa-btn fa-check text-success"></i>@{{ team.name }}--}}
            {{--</span>--}}

            {{--<span v-else>--}}
                {{--<img :src="team.photo_url" class="spark-team-photo-xs"><i class="fa fa-btn"></i>@{{ team.name }}--}}
            {{--</span>--}}
        {{--</a>--}}
    {{--</li>--}}
{{--@endif--}}

{{--<li class="divider"></li>--}}




<!-- Teams -->
<a class="dropdown-header dropdown-item" href="javascript:void(0)" role="menuitem">{{ ucfirst(str_plural(Spark::teamString())) }}</a>

<!-- Create Team -->
@if (Spark::createsAdditionalTeams())
        <a href="/settings#/{{str_plural(Spark::teamString())}}" class="dropdown-item" href="javascript:void(0)" role="menuitem">
            <i class="fa fa-fw fa-btn fa-plus"></i>Create {{ ucfirst(Spark::teamString()) }}
        </a>
@endif

<!-- Switch Current Team -->
@if (Spark::showsTeamSwitcher())
        <a v-for="team in teams" :href="'/{{ str_plural(Spark::teamString()) }}/'+ team.id +'/switch'" class="dropdown-item" href="javascript:void(0)" role="menuitem">
            <span v-if="user.current_team_id == team.id">
                <i class="fa fa-fw fa-btn fa-check text-success"></i>@{{ team.name }}
            </span>

            <span v-else>
                <img :src="team.photo_url" class="spark-team-photo-xs"><i class="fa fa-btn"></i>@{{ team.name }}
            </span>
        </a>
@endif

