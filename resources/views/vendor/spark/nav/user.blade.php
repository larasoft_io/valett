<!-- NavBar For Authenticated Users -->
{{--<spark-navbar--}}
{{--:user="user"--}}
{{--:teams="teams"--}}
{{--:current-team="currentTeam"--}}
{{--:has-unread-notifications="hasUnreadNotifications"--}}
{{--:has-unread-announcements="hasUnreadAnnouncements"--}}
{{--inline-template>--}}

{{--<nav class="navbar navbar-inverse navbar-fixed-top">--}}
{{--<div class="container" v-if="user">--}}
{{--<div class="navbar-header">--}}
{{--<!-- Collapsed Hamburger -->--}}
{{--<div class="hamburger">--}}
{{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"--}}
{{--data-target="#spark-navbar-collapse">--}}
{{--<span class="sr-only">Toggle Navigation</span>--}}
{{--<span class="icon-bar"></span>--}}
{{--<span class="icon-bar"></span>--}}
{{--<span class="icon-bar"></span>--}}
{{--</button>--}}
{{--</div>--}}

{{--<!-- Branding Image -->--}}
{{--@include('spark::nav.brand')--}}
{{--</div>--}}

{{--<div class="collapse navbar-collapse" id="spark-navbar-collapse">--}}
{{--<!-- Left Side Of Navbar -->--}}
{{--<ul class="nav navbar-nav">--}}
{{--@includeIf('spark::nav.user-left')--}}
{{--</ul>--}}

{{--<!-- Right Side Of Navbar -->--}}
{{--<ul class="nav navbar-nav navbar-right">--}}
{{--@includeIf('spark::nav.user-right')--}}

{{--<!-- Notifications -->--}}
{{--<li>--}}
{{--<a @click="showNotifications" class="has-activity-indicator">--}}
{{--<div class="navbar-icon">--}}
{{--<i class="activity-indicator" v-if="hasUnreadNotifications || hasUnreadAnnouncements"></i>--}}
{{--<i class="icon fa fa-bell"></i>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}

{{--<li class="dropdown">--}}
{{--<!-- User Photo / Name -->--}}
{{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
{{--<img :src="user.photo_url" class="spark-nav-profile-photo m-r-xs">--}}
{{--<span class="caret"></span>--}}
{{--</a>--}}

{{--<ul class="dropdown-menu" role="menu">--}}
{{--<!-- Impersonation -->--}}
{{--@if (session('spark:impersonator'))--}}
{{--<li class="dropdown-header">Impersonation</li>--}}

{{--<!-- Stop Impersonating -->--}}
{{--<li>--}}
{{--<a href="/spark/kiosk/users/stop-impersonating">--}}
{{--<i class="fa fa-fw fa-btn fa-user-secret"></i>Back To My Account--}}
{{--</a>--}}
{{--</li>--}}

{{--<li class="divider"></li>--}}
{{--@endif--}}

{{--<!-- Developer -->--}}
{{--@if (Spark::developer(Auth::user()->email))--}}
{{--@include('spark::nav.developer')--}}
{{--@endif--}}

{{--<!-- Subscription Reminders -->--}}
{{--@include('spark::nav.subscriptions')--}}

{{--<!-- Settings -->--}}
{{--<li class="dropdown-header">Settings</li>--}}

{{--<!-- Your Settings -->--}}
{{--<li>--}}
{{--<a href="/settings">--}}
{{--<i class="fa fa-fw fa-btn fa-cog"></i>Your Settings--}}
{{--</a>--}}
{{--</li>--}}

{{--<li class="divider"></li>--}}

{{--<!-- Settings -->--}}

{{--<!-- Your Settings -->--}}

{{--@if(auth()->user()->hasRole('buyer'))--}}
{{--<li class="dropdown-header">Menu</li>--}}
{{--<li>--}}
{{--<a href="/dashboard?table=mortgage">--}}
{{--<i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="{!! route('import.suppression', 1) !!}">--}}
{{--<i class="fa fa-file"></i> Upload Suppression File--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="/settings">--}}
{{--<i class="fa fa-user"></i> Profile--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#/orders">--}}
{{--<i class="fa fa-shopping-cart" aria-hidden="true"></i>  Orders--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#">--}}
{{--<i class="fa fa-book" aria-hidden="true"></i> Account--}}
{{--</a>--}}
{{--</li>--}}

{{--<li class="divider"></li>--}}
{{--@endif--}}


{{--@if (Spark::usesTeams() && (Spark::createsAdditionalTeams() || Spark::showsTeamSwitcher()))--}}
{{--<!-- Team Settings -->--}}
{{--@include('spark::nav.teams')--}}
{{--@endif--}}

{{--@if (Spark::hasSupportAddress())--}}
{{--<!-- Support -->--}}
{{--@include('spark::nav.support')--}}
{{--@endif--}}

{{--<!-- Logout -->--}}
{{--<li>--}}
{{--<a href="/logout">--}}
{{--<i class="fa fa-fw fa-btn fa-sign-out"></i>Logout--}}
{{--</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--</nav>--}}
{{--</spark-navbar>--}}


<spark-navbar
        :user="user"
        :teams="teams"
        :current-team="currentTeam"
        :has-unread-notifications="hasUnreadNotifications"
        :has-unread-announcements="hasUnreadAnnouncements"
        inline-template>

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                    data-toggle="menubar">
                <span class="sr-only">Toggle navigation</span>
                <span class="hamburger-bar"></span>
            </button>
            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                    data-toggle="collapse">
                <i class="icon wb-more-horizontal" aria-hidden="true"></i>
            </button>
            <div style="display: table-cell; vertical-align: middle;"
                 class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                <a style="text-decoration: none"
                   href="{!! auth()->user()->hasRole('buyer') ? '/dashboard?table='.array_keys(auth()->user()->getDBTables(auth()->user()))[0] : '/dashboard' !!}"
                   class="navbar-brand-text hidden-xs-down"><span
                            style="color:#62b451;text-align: center; font-size: 28px;"
                            class="navbar-brand-text">Valett</span></a>
            </div>
        </div>
        <div class="navbar-container container-fluid">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="nav-item hidden-float" id="toggleMenubar">
                        <a class="nav-link" data-toggle="menubar" href="#" role="button">
                            <div class="navbar-icon">
                                <i class="icon hamburger hamburger-arrow-left">
                                    <span class="sr-only">Toggle menubar</span>
                                    <span class="hamburger-bar"></span>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item dropdown dropdown-fw dropdown-mega">
                        <a @click="showNotifications" class="has-activity-indicator nav-link">
                        <div class="navbar-icon">
                            <i class="activity-indicator" v-if="hasUnreadNotifications || hasUnreadAnnouncements"></i>
                            <i class="icon fa fa-bell"></i>
                        </div>
                        </a>

                    </li>
                </ul>
                <!-- End Navbar Toolbar -->
                <!-- Navbar Toolbar Right -->
                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                           data-animation="scale-up" role="button">
                            <i style=" -webkit-transform: rotate(45deg);
          -moz-transform: rotate(90deg);
          -ms-transform: rotate(90deg);
          -o-transform: rotate(90deg);
          transform: rotate(90deg);
          display: inline-block;" class="icon ti-more-alt" aria-hidden="true"></i>
                        </a>
                        <div class="dropdown-menu" role="menu">

                            <!-- Impersonation -->
                            @if (session('spark:impersonator'))
                                <a class="dropdown-item dropdown-header" href="javascript:void(0)" role="menuitem">Impersonation</a>

                                <!-- Stop Impersonating -->

                                <a class="dropdown-item" role="menuitem" href="/spark/kiosk/users/stop-impersonating">
                                    <i class="fa fa-fw fa-btn fa-user-secret"></i>Back To My Account
                                </a>
                            @endif

                        <!-- Developer -->
                            @if (Spark::developer(Auth::user()->email))
                                @include('spark::nav.developer')
                            @endif

                        <!-- Subscription Reminders -->
                            @include('spark::nav.subscriptions')

                        <!-- Settings -->

                            <!-- Your Settings -->
                            <a class="dropdown-item" role="menuitem" href="/settings">
                                <i class="fa fa-fw fa-btn fa-cog"></i>Your Settings
                            </a>

                            <!-- Your Settings -->

                            @if (Spark::usesTeams() && (Spark::createsAdditionalTeams() || Spark::showsTeamSwitcher()))
                            <!-- Team Settings -->
                                @include('spark::nav.teams')
                            @endif

                            @if (Spark::hasSupportAddress())
                            <!-- Support -->
                                @include('spark::nav.support')
                            @endif

                            <div class="dropdown-divider" role="presentation"></div>

                            <!-- Logout -->
                            <a class="dropdown-item" href="/logout" role="menuitem"><i class="icon wb-power"
                                                                                       aria-hidden="true"></i>
                                Logout
                            </a>


                        </div>
                    </li>
                </ul>
                <!-- End Navbar Toolbar Right -->
            </div>
            <!-- End Navbar Collapse -->
            <!-- Site Navbar Seach -->
            <div class="collapse navbar-search-overlap" id="site-navbar-search">
                <form role="search">
                    <div class="form-group">
                        <div class="input-search">
                            <i class="input-search-icon wb-search" aria-hidden="true"></i>
                            <input type="text" class="form-control" name="site-search" placeholder="Search...">
                            <button type="button" class="input-search-close icon wb-close"
                                    data-target="#site-navbar-search"
                                    data-toggle="collapse" aria-label="Close"></button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End Site Navbar Seach -->
        </div>
    </nav>

</spark-navbar>