<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

    /*
     * Common Routes
     */
    Route::get('/', 'WelcomeController@show');

    Route::group(['middleware' => ['auth']], function (){

        Route::get('/dashboard', 'HomeController@show');

        Route::post('store/settings', ['as' => 'settings.store', 'uses' => 'HomeController@store_settings']);

        Route::post('sidebar/settings', ['as' => 'sidebar.settings', 'uses' => 'UsersController@sidebarSettings']);

        /*
         * Vendor Routes
         */
        Route::group(['middleware' => ['role:vendor'], 'prefix' => 'vendor'], function ()
        {
            Route::get('import/data/{step}', ['as' => 'vendor.import.data', 'uses' => 'ImportDataController@import']);

            Route::post('import/store/step1', ['as' => 'vendor.import.step1', 'uses' => 'ImportDataController@storeStep1']);
            Route::post('import/store/step2', ['as' => 'vendor.import.step2', 'uses' => 'ImportDataController@storeStep2']);
            Route::get('import/tableCheck', ['as' => 'vendor.import.checkDatabase', 'uses' => 'ImportDataController@checkDatabaseName']);
            Route::get('orders', 'OrdersController@vendor_orders');
        });

        /*
         * Buyer Routes
         */
        Route::group(['middleware' => ['role:buyer|reseller']], function ()
        {
            Route::get('import/suppression/{step}', ['as' => 'import.suppression', 'uses' => 'ImportSuppressionController@import']);

            Route::post('store/suppression/step1', ['as' => 'suppression.step1', 'uses' => 'ImportSuppressionController@storeStep1']);
            Route::post('store/suppression/step2', ['as' => 'suppression.step2', 'uses' => 'ImportSuppressionController@storeStep2']);
            Route::post('store/queries', ['as' => 'store.queries', 'uses' => 'UsersController@saveQuery']);
            Route::get('search/queries', ['as' => 'search.queries', 'uses' => 'UsersController@searchQuery']);
            Route::get('previous/queries', ['as' => 'previous.queries', 'uses' => 'UsersController@previousQueryData']);
            Route::get('pricing/range', ['as' => 'pricing.range', 'uses' => 'UsersController@getPriceRange']);

            Route::post('checkout/step1', ['as' => 'checkout.step1', 'uses' => 'CheckoutController@storeCheckoutStep1']);
            Route::post('checkout/step2', ['as' => 'checkout.step2', 'uses' => 'CheckoutController@storeCheckoutStep2']);
            Route::get('checkout/data', ['as' => 'checkout.data', 'uses' => 'CheckoutController@getCheckoutRecords']);

            Route::post('checkout/step3', ['as' => 'checkout.step3', 'uses' => 'CheckoutController@storeCheckoutStep3']);

            Route::post('records', ['as' => 'records', 'uses' => 'CheckoutController@getRecords']);

            Route::get('table-fields', 'UsersController@getFieldsFromTable');

            Route::get('records-count-cost', 'CheckoutController@getRecordsCountCost');

            Route::get('check/previous/orders', 'CheckoutController@checkPreviousOrders');

            Route::get('orders', 'OrdersController@index');

            Route::get('invoice', 'OrdersController@invoice');

            Route::get('orders/{id}/file/download', 'OrdersController@downloadFile');

            Route::get('/search', 'UsersController@search');

            Route::get('vendor/options', 'UsersController@getVendors');

            Route::get('suppression/files', 'ImportSuppressionController@getSuppressionFiles');

            Route::post('/invite/buyers',  'UsersController@inviteBuyer');


        });

        /*
         * Admin Routes
         */
        Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin'], function ()
        {
            Route::get('vendors', 'UsersController@getVendorUsers');
            Route::put('vendors/{id}/update', 'UsersController@updateVendor');
        });


    });
