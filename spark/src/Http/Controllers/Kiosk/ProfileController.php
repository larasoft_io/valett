<?php

namespace Laravel\Spark\Http\Controllers\Kiosk;

use App\User;
use Illuminate\Support\Facades\Log;
use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Repositories\UserRepository;
use Laravel\Spark\Contracts\Repositories\PerformanceIndicatorsRepository;

class ProfileController extends Controller
{
    /**
     * The performance indicators repository instance.
     *
     * @var PerformanceIndicatorsRepository
     */
    protected $indicators;

    /**
     * Create a new controller instance.
     *
     * @param  PerformanceIndicatorsRepository $indicators
     * @return void
     */
    public function __construct(PerformanceIndicatorsRepository $indicators)
    {
        $this->indicators = $indicators;

        $this->middleware('auth');
        $this->middleware('dev');
    }

    /**
     * Get the user to be displayed on the user profile screen.
     *
     * @param  Request $request
     * @param  string $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $user = Spark::call(UserRepository::class . '@find', [$id]);

        return response()->json([
            'user' => $user,
            'revenue' => $this->indicators->totalRevenueForUser($user),
        ]);
    }

    public function storePricing(Request $request, $id)
    {
        $user = User::find($id);

        $user->update([
            'price_0_to_7_days_old' => $request->price_0_to_7_days_old,
            'price_7_to_14_days_old' => $request->price_7_to_14_days_old,
            'price_14_to_30_days_old' => $request->price_14_to_30_days_old,
            'price_greater_than_30_days_old' => $request->price_greater_than_30_days_old
        ]);

    }
}
